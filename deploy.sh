#!/bin/bash
set -e

S3_BUCKET=""
REGION="us-east-1"
PREFIX="aws"

function usage()
{
    echo "Billing Analytics Deployment Script"
    echo ""
    echo "$0"
    echo "\t-h --help"
    echo "\t--s3-bucket=<bucket>"
    echo "\t--prefix=${PREFIX}"
    echo "\t--region=${REGION}"
    echo ""
}

while [ "$1" != "" ]; do
    PARAM=`echo $1 | awk -F= '{print $1}'`
    VALUE=`echo $1 | awk -F= '{print $2}'`
    case $PARAM in
        -h | --help)
            usage
            exit
            ;;
        --prefix)
            PREFIX=$VALUE
            ;;
        --s3-bucket)
            S3_BUCKET=$VALUE
            ;;
        --region)
            REGION=$VALUE
            ;;
        *)
            echo "ERROR: unknown parameter \"$PARAM\""
            usage
            exit 1
            ;;
    esac
    shift
done
if [[ -z $S3_BUCKET ]];
then
  echo
  echo "ERROR: --s3-bucket is required."
  echo 
  usage
  exit 1
fi

# Use docker to cross compile pip packages into the vendored folders
docker run -v `pwd`:/opt lambci/lambda:build-python3.6 -w /opt /opt/build_deployment_files.sh

aws s3 sync  --exclude '*.placeholder' ./deployment/ s3://${S3_BUCKET}/${PREFIX}_billing_analytics_deployment/

echo "Login to your AWS account and launch stack: https://console.aws.amazon.com/cloudformation/home?region=${REGION}#/stacks/new?stackName=${PREFIX}-billing-analytics&templateURL=https://s3.amazonaws.com/${S3_BUCKET}/${PREFIX}_billing_analytics_deployment/templates/squeegee.yaml"
