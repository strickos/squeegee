import sys
import os
sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/vendored')
from datetime import datetime
from uuid import uuid4
import pyarrow.parquet as pq
import pyarrow as pa
import pandas as pd
import requests
import s3fs
import boto3
import logging

logging.basicConfig()

output_bucket = os.environ.get('ANALYTICS_BUCKET')
output_bucket_region = os.environ.get('ANALYTICS_BUCKET_REGION')
output_prefix = os.environ.get('S3_KEY_PREFIX')
function_region = os.environ.get('AWS_REGION')
glue_crawler_name = os.environ.get('GLUE_CRAWLER_NAME')
glue_crawler_region = os.environ.get('GLUE_CRAWLER_REGION')
glue_management_arn = os.environ.get('GLUE_MANAGEMENT_ROLE')
glue_role_arn = os.environ.get('GLUE_CRAWLER_ROLE')
glue_crawler_prefix = os.environ.get('GLUE_CRAWLER_PREFIX')
glue_s3_target = 's3://' + output_bucket + '/' + output_prefix
datacatalog_db_name = os.environ.get('DATACATALOG_DB_NAME')


def fix_column_names(df):
    new_columns = list()
    for col in df.columns:
        new_name = col.lower().replace('/', '_').replace(':', '_').replace(' ', '_')

        # Check if this new column name clashes with any others, if it does add number to the end of column name
        i = 0
        while new_name in new_columns:
            new_name = col.lower().replace('/', '_').replace(':', '_').replace(' ', '_') + str(i)
            i += 1

        new_columns.append(new_name)
    # Set the new column names on the DF
    df.columns = new_columns
    return df


def upsert_crawler(glue, logger):
    args = {
        'Name': glue_crawler_name,
        'Role': glue_role_arn,
        'DatabaseName': datacatalog_db_name,
        'TablePrefix': glue_crawler_prefix,
        'Description': 'Cost and Usage Report Data Crawler',
        'Targets': {'S3Targets': [{"Exclusions": [], "Path": glue_s3_target}]},
        'SchemaChangePolicy': {'DeleteBehavior': 'DELETE_FROM_DATABASE',
                               'UpdateBehavior': 'UPDATE_IN_DATABASE'}
    }
    try:
        response = glue.update_crawler(**args)
    except Exception:
        logger.info('Crawler {} not found, creating new one.'.format(glue_crawler_name))
        response = glue.create_crawler(**args)
    return response


def run_crawler(logger):
    sts = boto3.client(service_name='sts', region_name=function_region)
    credentials = sts.assume_role(RoleArn=glue_management_arn, RoleSessionName='CURDataTransformLambda')
    glue = boto3.client(service_name='glue',
                        region_name=glue_crawler_region,
                        aws_session_token=credentials['Credentials']['SessionToken'],
                        aws_access_key_id=credentials['Credentials']['AccessKeyId'],
                        aws_secret_access_key=credentials['Credentials']['SecretAccessKey'])

    if upsert_crawler(glue, logger):
        start_crawler(glue)
    else:
        logger.error('Upsert crawler failed - refusing to run crawler')
        return False
    return True


def start_crawler(glue):
    # Start glue crawler
    return glue.start_crawler(Name=glue_crawler_name)


def handler(event, context):
    # Initialise Logging
    logger = logging.getLogger()
    logger.handlers = []
    h = logging.StreamHandler(sys.stdout)
    if 'uuid' in event:
        uuid = event['uuid']
    else:
        uuid = str(uuid4())
    log_format = '%(asctime)s - ' + context.aws_request_id + ' - ' + uuid + ' - %(name)s - %(levelname)s - %(message)s'
    h.setFormatter(logging.Formatter(log_format))
    logger.addHandler(h)
    logger.setLevel(logging.DEBUG)
    ######

    url_base = 'https://pricing.us-east-1.amazonaws.com'
    offers_url = '{base}/offers/v1.0/aws/index.json'.format(base=url_base)
    r = requests.get(offers_url)

    df = pd.DataFrame()
    for offer_code, offer in r.json()['offers'].items():
        tmp = pd.read_csv('{base}{url}'.format(base=url_base, url=offer['currentVersionUrl'].replace('.json', '.csv')),
                          skiprows=5,
                          dtype=object)
        tmp.offer_code = offer_code
        tmp = fix_column_names(tmp)
        df = df.append(tmp)

    table = pa.Table.from_pandas(df)

    s3 = s3fs.S3FileSystem(anon=False, s3_additional_kwargs={'region_name': output_bucket_region})
    year = datetime.utcnow().year
    month = datetime.utcnow().month
    day = datetime.utcnow().day
    with s3.open('{bucket}/{prefix}/year={year}/month={month}/day={day}/pricelist.parquet'.format(bucket=output_bucket,
                                                                                                  prefix=output_prefix,
                                                                                                  year=year,
                                                                                                  month=month,
                                                                                                  day=day), 'wb') as f:
        pq.write_table(table, f)
    run_crawler(logger)
