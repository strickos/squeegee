def apply_df_transforms(df, mappings):
    if 'resourcetags_user_business_unit' in df and 'bad_business_unit_mapping' in mappings and 'default_cost_centers' in mappings:  # noqa
        df['default_business_unit'] = df['lineitem_usageaccountid'].map(mappings['default_cost_centers'], na_action='')
        df['resourcetags_user_cost_allocation'] = df['resourcetags_user_business_unit']
        df['resourcetags_user_cost_allocation'] = df['resourcetags_user_cost_allocation'].combine_first(df['default_business_unit'])  # noqa

        df['resourcetags_user_cost_allocation'] = df['resourcetags_user_cost_allocation'].map(mappings['bad_business_unit_mapping'], na_action='ignore').combine_first(df['resourcetags_user_cost_allocation'])  # noqa

    if 'resourcetags_user_service_cost_group' in df:
        if 'resourcetags_user_micros_service_id' in df:
            df['resourcetags_user_service_cost_group'] = df['resourcetags_user_service_cost_group'].combine_first(df['resourcetags_user_micros_service_id'])  # noqa
        if 'resourcetags_user_service_name' in df:
            df['resourcetags_user_service_cost_group'] = df['resourcetags_user_service_cost_group'].combine_first(df['resourcetags_user_service_name'])  # noqa
    else:
        df['resourcetags_user_service_cost_group'] = ''

    for key in [col for col in df.columns if col.startswith('resourcetags_user_')]:
        df[key] = df[key].str.strip()

    if 'free_tier_descriptions' in mappings and 'lineitem_lineitemdescription' in df:
        df['is_free_tier'] = df['lineitem_lineitemdescription'].map(
            mappings['free_tier_descriptions'], na_action='ignore')
        df['is_free_tier'] = df['is_free_tier'].fillna('false')

    if 'pricing_publicondemandrate' in df and 'lineitem_unblendedrate' in df and 'lineitem_lineitemtype' in df and 'product_productname' in df:
        # This is to fix up some services lines as they publish zero value rates
        df.loc[df['pricing_publicondemandcost'] == '0.0000000000', 'pricing_publicondemandcost'] = None
        df.loc[df['pricing_publicondemandrate'] == '0.0000000000', 'pricing_publicondemandrate'] = None

        # This will copy the public ondemand rate/cost if its not filled in use our actual rate/cost
        df['shelf_rate'] = df['pricing_publicondemandrate'].combine_first(df['lineitem_unblendedrate'])
        df['shelf_cost'] = df['pricing_publicondemandcost'].combine_first(df['lineitem_unblendedcost'])

    if 'reservation_reservationarn' in df and 'pricing_publicondemandcost' in df and 'pricing_publicondemandrate' in df and 'lineitem_unblendedrate' in df and 'lineitem_unblendedcost' in df and 'lineitem_lineitemtype' in df:
        df.loc[(df['reservation_reservationarn'].notnull()) | (df['lineitem_lineitemtype'] == 'DiscountedUsage'), 'unblendedcost_withoutri'] = df['pricing_publicondemandcost']
        df['unblendedcost_withoutri'] = df['unblendedcost_withoutri'].combine_first(df['lineitem_unblendedcost'])
        df.loc[df['lineitem_lineitemtype'].isin(['RIFee']), 'unblendedcost_withoutri'] = '0.0000000000'
        df.loc[(df['lineitem_lineitemtype'].isin(['Fee'])) & (df['reservation_reservationarn'].notnull()), 'unblendedcost_withoutri'] = '0.0000000000'

        df.loc[(df['reservation_reservationarn'].notnull()) | (df['lineitem_lineitemtype'] == 'DiscountedUsage'), 'unblendedrate_withoutri'] = df['pricing_publicondemandrate']
        df['unblendedrate_withoutri'] = df['unblendedrate_withoutri'].combine_first(df['lineitem_unblendedrate'])
        df.loc[df['lineitem_lineitemtype'].isin(['RIFee']), 'unblendedrate_withoutri'] = '0.0000000000'
        df.loc[(df['lineitem_lineitemtype'].isin(['Fee'])) & (df['reservation_reservationarn'].notnull()), 'unblendedrate_withoutri'] = '0.0000000000'

    if 'airport_codes' in mappings and 'product_region' in df:
        df['airport_code'] = df['product_region'].map(
            mappings['airport_codes'], na_action='ignore')

    if 'geo_locations' in mappings and 'product_region' in df:
        df['geo_location'] = df['product_region'].map(
            mappings['geo_locations'], na_action='ignore')
