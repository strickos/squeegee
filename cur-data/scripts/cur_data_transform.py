import sys
import os
sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/vendored')
from vendored.gevent import pool
from vendored.gevent import monkey
monkey.patch_all()
import boto3
import json
import logging
import pandas as pd
import pyarrow.parquet as pq
import pyarrow as pa
import re
import s3fs
from botocore.client import Config
from collections import defaultdict
from mapping_handler import stage_mapping_files, load_mappings
from time import sleep
from transformations import apply_df_transforms
from uuid import uuid4
from time import sleep


logging.basicConfig()

# This was a nice idea but until PyArrow and Athena get more supported types it
# is useless for us. I'll leave it here in dreams of one day using it.
column_format_mapping = defaultdict(pa.string)

cur_bucket = os.environ.get('CUR_BUCKET', 'ci-test')
cur_bucket_region = os.environ.get('CUR_BUCKET_REGION', 'us-west-1')
output_bucket = os.environ.get('ANALYTICS_BUCKET', 'ci-test-output')
output_bucket_region = os.environ.get('ANALYTICS_BUCKET_REGION', 'us-west-1')
output_prefix = os.environ.get('S3_KEY_PREFIX', 'aws_cost_usage_report')
manifest_key_regex = r'.*\/?(?P<year>[0-9]{4})(?P<month>[0-9]{2})(?P<day>[0-9]{2})-[0-9]{8}\/[^\/]*\.json$'
function_region = os.environ.get('AWS_REGION', 'us-east-1')
glue_crawler_name = os.environ.get('GLUE_CRAWLER_NAME')
glue_crawler_region = os.environ.get('GLUE_CRAWLER_REGION')
glue_management_arn = os.environ.get('GLUE_MANAGEMENT_ROLE')
glue_role_arn = os.environ.get('GLUE_CRAWLER_ROLE')
glue_crawler_prefix = os.environ.get('GLUE_CRAWLER_PREFIX')
glue_s3_target = 's3://' + output_bucket + '/' + output_prefix
datacatalog_db_name = os.environ.get('DATACATALOG_DB_NAME')
mappings_prefix = os.environ.get('MAPPING_FILES_PREFIX', 'mappings')


def get_bucket_details_from_event(event):
    result = (None, None)
    if 'Records' in event:
        for record in event['Records']:
            if 'Sns' in record and 'Message' in record['Sns']:
                return get_bucket_details_from_event(json.loads(record['Sns']['Message']))
            elif 's3' in record:
                if 'bucket' in record['s3'] and \
                   'name' in record['s3']['bucket'] and \
                   'object' in record['s3'] and \
                   'key' in record['s3']['object']:
                    result = (record['s3']['bucket']['name'], record['s3']['object']['key'])
    return result


def validate_manifest_key(manifest_key):
    if re.match(manifest_key_regex, manifest_key):
        return True
    return False


def get_manifest(cur_bucket, cur_bucket_region, manifest_key):
    if manifest_key.endswith('.json'):
        s3 = s3fs.S3FileSystem(anon=False, s3_additional_kwargs={'region_name': cur_bucket_region})
        in_file = 's3://{bucket}/{key}'.format(bucket=cur_bucket, key=manifest_key)
        with s3.open(in_file, 'rb') as f:
            return json.loads(f.read())
    else:
        return None


def get_year_month_from_manifest_key(manifest_key):
    g = re.match(manifest_key_regex, manifest_key)
    if g:
        return g.group('year'), g.group('month')
    return None, None


def generate_transform_list(report_keys, function_name, function_alias, year, month, dest_path, uuid):
    transform_list = list()
    if len(report_keys) > 20:
        for key_set in [report_keys[i:i + 20] for i in range(0, len(report_keys), 20)]:
            payload = {'Workset': generate_transform_list(key_set,
                                                          function_name,
                                                          function_alias,
                                                          year,
                                                          month,
                                                          dest_path,
                                                          uuid),
                       'uuid': uuid}
            transform_list.append({"FunctionName": function_name,
                                   "Payload": payload,
                                   "Qualifier": function_alias
                                   })
    else:
        transform_list = list()
        for cur_file_key in report_keys:
            output_filename = cur_file_key.split('/')[-1].replace('csv.gz', 'parquet')
            payload = {
                'uuid': uuid,
                'Source': {
                    'Bucket': cur_bucket,
                    'Key': cur_file_key,
                    'Region': cur_bucket_region
                },
                'Destination': {
                    'Bucket': output_bucket,
                    'Key': '{prefix}/{filename}'.format(prefix=dest_path,
                                                        filename=output_filename),
                    'Region': output_bucket_region
                },
                'Year': year,
                'Month': month
            }
            kwargs = {"FunctionName": function_name,
                      "Payload": payload,
                      "Qualifier": function_alias
                      }
            transform_list.append(kwargs)
    return transform_list


def invoke_transformations(transform_list, logger):
    logger.info('Running transform list of {count} items'.format(count=len(transform_list)))
    client = boto3.client('lambda', region_name=function_region, config=Config(connect_timeout=300, read_timeout=300))
    g = pool.Group()

    def unpack(k):
        logger.info('Invoking with event {event}'.format(event=k))
        k['Payload'] = json.dumps(k['Payload']).encode('utf-8')
        sleep(0.025)
        resp = client.invoke(**k)
        logger.info('Finished invoke')
        return resp

    results = g.imap_unordered(unpack, transform_list)
    g.join()
    return list(results)


def transform_file(event, mappings, logger):
    try:
        source_bucket = event['Source']['Bucket']
        source_bucket_region = event['Source']['Region']
        source_key = event['Source']['Key']
        destination_bucket = event['Destination']['Bucket']
        destination_bucket_region = event['Destination']['Region']
        destination_key = event['Destination']['Key']

        in_file = 's3://{bucket}/{key}'.format(bucket=source_bucket,
                                               key=source_key)
        logger.info('Reading from: {file}'.format(file=in_file))

        out_file = 's3://{bucket}/{out_file}'.format(bucket=destination_bucket,
                                                     out_file=destination_key)
        logger.info('Writing to: {file}'.format(file=out_file))

        s3_in = s3fs.S3FileSystem(anon=False, s3_additional_kwargs={'region_name': source_bucket_region})
        s3_out = s3fs.S3FileSystem(anon=False, s3_additional_kwargs={'region_name': destination_bucket_region})

        # Sometimes we get 404 errors from s3, we need to wait for AWS to complete the upload.
        while not s3_in.exists(in_file):
            logger.warn('File {file} does not exist yet, waiting...'.format(file=in_file))
            sleep(5)

        with s3_in.open(in_file, 'rb') as f_in, s3_out.open(out_file, 'wb') as f_out:
            # Chunksize chosen arbitrarily, over 200K seems to blow out the memory usage.
            # over 100K doesn't seem to decrease duration, but having chunks increase
            # parquet compression up to 64 * 1024 * 1024
            df = pd.read_csv(f_in, skiprows=0, compression='gzip', dtype=object, iterator=True, chunksize=100000)
            # Write out to S3.
            write_parquet(df, f_out, mappings, logger)
    except Exception as e:
        logger.exception(e)
        return False
    return True


def fix_column_names(df):
    # This is used by Atlassian to clean up some older tags
    for col in ['resourceTags/user:Business_Unit',
                'resourceTags/user:Business_unit',
                'resourceTags/user:Department',
                'resourceTags/user:Environment',
                'resourceTags/user:Owner',
                'resourceTags/user:Service',
                'resourceTags/user:name',
                'resourceTags/user:owner']:
        if col in df:
            df = df.drop(col, 1)

    new_columns = list()
    for col in df.columns:
        new_name = col.lower().replace('/', '_').replace(':', '_').replace(' ', '_')
        
        # Check if this new column name clashes with any others, if it does add number to the end of column name
        i = 0
        while new_name in new_columns:
            new_name = col.lower().replace('/', '_').replace(':', '_').replace(' ', '_') + str(i)
            i += 1

        new_columns.append(new_name)
    # Set the new column names on the DF
    df.columns = new_columns
    return df


def write_parquet(in_file, out_file, mappings, logger):
    writer = None
    count = 0
    for chunk in in_file:
        logger.debug('DF Chunk Memory Usage: %d', chunk.memory_usage(index=True).sum())
        count += 1
        logger.info('Reading chunk ({id})'.format(id=count))
        chunk = fix_column_names(chunk)
        apply_df_transforms(chunk, mappings)

        tb = pa.Table.from_pandas(chunk, preserve_index=False)
        if not writer:
            columns = [pa.field(c, column_format_mapping[c]) for c in chunk.columns]
            schema = pa.schema(columns)
            writer = pq.ParquetWriter(out_file, schema, compression='snappy')
        logger.info('Writing chunk ({id})'.format(id=count))
        writer.write_table(tb)
    writer.close()


def upsert_crawler(glue, logger):
    args = {
        'Name': glue_crawler_name,
        'Role': glue_role_arn,
        'DatabaseName': datacatalog_db_name,
        'TablePrefix': glue_crawler_prefix,
        'Description': 'Cost and Usage Report Data Crawler',
        'Targets': {'S3Targets': [{"Exclusions": [], "Path": glue_s3_target}]},
        'SchemaChangePolicy': {'DeleteBehavior': 'DELETE_FROM_DATABASE',
                               'UpdateBehavior': 'UPDATE_IN_DATABASE'}
    }
    try:
        response = glue.update_crawler(**args)
    except Exception:
        logger.info('Crawler {} not found, creating new one.'.format(glue_crawler_name))
        response = glue.create_crawler(**args)
    return response


def run_crawler(logger):
    sts = boto3.client(service_name='sts', region_name=function_region)
    credentials = sts.assume_role(RoleArn=glue_management_arn, RoleSessionName='CURDataTransformLambda')
    glue = boto3.client(service_name='glue',
                        region_name=glue_crawler_region,
                        aws_session_token=credentials['Credentials']['SessionToken'],
                        aws_access_key_id=credentials['Credentials']['AccessKeyId'],
                        aws_secret_access_key=credentials['Credentials']['SecretAccessKey'])

    if upsert_crawler(glue, logger):
        start_crawler(glue)
    else:
        logger.error('Upsert crawler failed - refusing to run crawler')
        return False
    return True


def start_crawler(glue):
    # Start glue crawler
    return glue.start_crawler(Name=glue_crawler_name)


def check_results(results):
    errors = list()
    for result in results:
        response = json.loads(result['Payload'].read().decode())
        if 'result' not in response or not response['result']:
            errors.append(response)
    return errors


def split_workset(event, function_name, function_alias, uuid):
    workset = list()
    for parts in [event['Workset'][i:i + 5] for i in range(0, len(event['Workset']), 5)]:
        workset.append({"FunctionName": function_name,
                        "Payload": {"Workset": parts, 'uuid': uuid },
                        "Qualifier": function_alias
                        })
    return workset


def validate_generated_files(input_bucket_name,
                             output_bucket_name,
                             output_bucket_region,
                             input_filepath,
                             output_filepath,
                             logger):
    try:
        s3 = boto3.resource('s3', region_name=output_bucket_region)

        in_bucket = s3.Bucket(input_bucket_name)
        out_bucket = s3.Bucket(output_bucket_name)

        if not input_filepath.endswith('/'):
            input_filepath = input_filepath + '/'

        if not output_filepath.endswith('/'):
            output_filepath = output_filepath + '/'

        in_files = {o.key.split('/')[-1].split('.')[0] for o in in_bucket.objects.filter(Prefix=input_filepath,
                                                                                         Delimiter='/')
                    if 'Manifest' not in o.key}
        out_files = {o.key.split('/')[-1].split('.')[0] for o in out_bucket.objects.filter(Prefix=output_filepath,
                                                                                           Delimiter='/')
                     if o.meta.data['Size'] > 0}
        return len(in_files.difference(out_files)) == 0
    except Exception as e:
        logger.error(e)
        return False


def chunks(chunkable, n):
    for i in range(0, len(chunkable), n):
        yield chunkable[i:i+n]


def clean_out_s3_folder(bucket_name, bucket_region, prefix): 
    if not prefix.endswith('/'):
        prefix = prefix + '/'

    s3 = boto3.resource('s3', region_name=bucket_region)
    bucket = s3.Bucket(bucket_name)
    for chunk in chunks([{'Key': f.key} for f in bucket.objects.filter(Prefix=prefix)], 1000):
        bucket.delete_objects(Delete={'Objects': chunk})


def handler(event, context):
    # Initialise Logging
    logger = logging.getLogger()
    logger.handlers = []
    h = logging.StreamHandler(sys.stdout)
    if 'uuid' in event:
        uuid = event['uuid']
    else:
        uuid = str(uuid4())
    log_format = '%(asctime)s - ' + context.aws_request_id + ' - ' + uuid + ' - %(name)s - %(levelname)s - %(message)s'
    h.setFormatter(logging.Formatter(log_format))
    logger.addHandler(h)
    logger.setLevel(logging.DEBUG)
    ######

    function_name = context.function_name
    function_alias = context.function_version
    return_result = {'result': False}
    if 'Records' in event:
        bucket_name, manifest_key = get_bucket_details_from_event(event)
        
        if bucket_name != cur_bucket:
            logger.error('Bucket {} does not match expected bucket {}'.format(bucket_name, cur_bucket))
            return return_result
        
        if not validate_manifest_key(manifest_key):
            logger.info('{} not a file we are interested in'.format(manifest_key))
            return return_result
    
        year, month = get_year_month_from_manifest_key(manifest_key)
        if not year or not month:
            logger.error('Unable to get year/month from manifest key')
            return return_result

        # Load mapping files into partition folder if current month being handled
        stage_mapping_files(year, month, output_bucket, output_bucket_region, mappings_prefix)

        dest_path = '{prefix}/month={year}-{month}'.format(prefix=output_prefix, year=year, month=month)
        manifest = get_manifest(cur_bucket, cur_bucket_region, manifest_key)
        transform_list = generate_transform_list(manifest['reportKeys'],
                                                 function_name,
                                                 function_alias,
                                                 year,
                                                 month,
                                                 dest_path,
                                                 uuid)
        clean_out_s3_folder(output_bucket, output_bucket_region, dest_path)
        results = invoke_transformations(transform_list, logger)
        input_filepath = manifest['reportKeys'][0].rsplit('/', 1)[0]

        if check_results(list(results)) and validate_generated_files(cur_bucket,
                                                                     output_bucket,
                                                                     output_bucket_region,
                                                                     input_filepath,
                                                                     dest_path,
                                                                     logger):
            logger.error('Processing had failures')
            # TODO: Consider retry?

        return_result['result'] = run_crawler(logger)
    elif 'Source' in event and 'Destination' in event and 'Year' in event and 'Month' in event:
        year = event['Year']
        month = event['Month']
        mappings = load_mappings(year, month, output_bucket, output_bucket_region, mappings_prefix)
        result = transform_file(event, mappings, logger)
        if not result:
            logger.error('Failed to handle file {}'.format(event['Source']))
            return_result['Source'] = event['Source']
            return return_result
        logger.info(
            'Finished with {remaining_time} ms remaining'.format(remaining_time=context.get_remaining_time_in_millis()))
        return_result = {'result': True, 'Source': event['Source']}
    elif 'Workset' in event:
        if len(event['Workset']) > 5:
            event['Workset'] = split_workset(event, function_name, function_alias, uuid)
        results = invoke_transformations(event['Workset'], logger)
        if check_results(list(results)):
            logger.error('Failed to process workset')
            # TODO: Consider retry?
            return return_result
    else:
        logger.error('Unknown event: {}'.format(event))
        return return_result
    return_result['result'] = True
    return return_result

