from cur_data_transform import *
import pandas as pd


def test_bucket_details_from_event_empty():
    event = {
        "Records": []
    }
    assert get_bucket_details_from_event(event) == (None, None)


def test_bucket_details_from_event_sns():
    event = {
        "Records": [
            {
                "Sns": {
                    "Message": """{ "Records": [
                                        {
                                            "s3": {
                                                "bucket": {"name": "foo"},
                                                "object": {"key": "bar"}
                                            }
                                        }
                                    ]
                                }"""
                }
            }
        ]
    }
    assert get_bucket_details_from_event(event) == ('foo', 'bar')


def test_bucket_details_from_event_s3():
    event = {
        "Records": [
            {
                "s3": {
                    "bucket": {"name": "foo"},
                    "object": {"key": "bar"}
                }
            }
        ]
    }
    assert get_bucket_details_from_event(event) == ('foo', 'bar')


def test_validate_manifest_key_valid():
    assert validate_manifest_key(
        'cost_usage_report/20171001-20171101/hourly_with_resources-Manifest.json')
    assert validate_manifest_key(
        '20171001-20171101/hourly_with_resources-Manifest.json')


def test_validate_manifest_key_invalid():
    assert not validate_manifest_key(
        'cost_usage_report/20171001-20171101/hourly_with_resources-Manifest.csv')
    assert not validate_manifest_key(
        'cost_usage_report/20171001a-20171101/hourly_with_resources-Manifest.json')
    assert not validate_manifest_key(
        '20171001-20171s101/hourly_with_resources-Manifest.json')
    assert not validate_manifest_key(
        'cost_usage_report/20171001-20171101/jikius-kjhkjhk-ykyx-skhkjh/hourly_with_resources-Manifest.json')


def test_get_year_month_from_manifest_key():
    assert get_year_month_from_manifest_key(
        'cost_usage_report/20171001-20171101/hourly_with_resources-Manifest.json'
    ) == ('2017', '10')
    assert get_year_month_from_manifest_key(
        'cost_usage_report/20170901-20171001/hourly_with_resources-Manifest.json'
    ) == ('2017', '09')
    assert get_year_month_from_manifest_key(
        'cost_usage_report/20181001-20181101/hourly_with_resources-Manifest.json'
    ) == ('2018', '10')
    assert get_year_month_from_manifest_key(
        'cost_usage_report/20170101-20170201/hourly_with_resources-Manifest.json'
    ) == ('2017', '01')


def test_fix_column_names():
    df = pd.DataFrame.from_dict({
        'resourceTags/user:Name': ['test', 'test2'],
        'resourceTags/user:Business_unit': ['bu1', 'bu2']
    })
    df = fix_column_names(df)
    assert list(df.columns) == ['resourcetags_user_name']
