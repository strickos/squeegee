---
AWSTemplateFormatVersion: "2010-09-09"

Description: |
  Billing Analytics Cost and Usage Data Handler

Parameters:
  LambdaRoleARN:
    Description: The ARN of the role to use for Lambda
    Type: String
  LambdaRoleID:
    Description: The id of the role to use for Lambda
    Type: String
  LambdaMemory:
    Description: The amount of memory to allocate the CUR Data Transform Lambda
    Type: Number
    Default: 1536
  CURBucket:
    Description: The S3 bucket to read the CUR data from
    Type: String
  CURBucketRegion:
    Description: The S3 bucket region to read the CUR data from
    Type: String
  AnalyticsBucket:
    Description: The S3 bucket to load the data into
    Type: String
  AnalyticsBucketRegion:
    Description: The S3 bucket region to load the data into
    Type: String
  KeyPrefix:
    Description: The key prefix to use for the S3 path
    Type: String
  GlueCrawlerRegion:
    Description: The region to deploy the Glue crawler in
    Type: String
  GlueCrawlerName:
    Description: The name to give the Glue crawler
    Type: String
  GlueCrawlerPrefix:
    Description: The table prefix to use for datacatalog table names
    Type: String
  GlueDataCatalogName:
    Description: The name of the datacatalog
    Type: String
  GlueManagementRoleARN:
    Description: The ARN of the role to use for managing Glue
    Type: String
  GlueCrawlerRoleARN:
    Description: The ARN of the role to use for Glue Crawlers
    Type: String
  SqueegeeS3BucketName:
    Description: The S3 Bucket containing the Squeegee Code and Templates
    Type: String
    Default: dbr-pp-deployments
  SqueegeeS3KeyPrefix:
    Description: The ARN of the role to use for Glue Crawlers
    Type: String
    Default: ""

Mappings: {}

Conditions:
  DeploySNSTopic: !Equals [!Ref "AWS::Region", !Ref CURBucketRegion]

Resources:
  CURDataLambdaPolicy:
    Type: AWS::IAM::Policy
    Properties:
      Roles:
        - !Ref LambdaRoleID
      PolicyName: CURDataLambdaPolicy
      PolicyDocument:
        Version: "2012-10-17"
        Statement:
          - Action:
              - "lambda:InvokeFunction"
            Effect: "Allow"
            Resource:
              - !GetAtt CurDataTransform.Arn
          - Action:
              - "s3:Get*"
              - "s3:List*"
            Effect: "Allow"
            Resource:
              - !Sub "arn:aws:s3:::${CURBucket}"
              - !Sub "arn:aws:s3:::${CURBucket}/*"
          - Action:
              - "s3:Get*"
              - "s3:List*"
              - "s3:PutObject"
              - "s3:DeleteObject"
            Effect: "Allow"
            Resource:
              - !Sub "arn:aws:s3:::${AnalyticsBucket}"
              - !Sub "arn:aws:s3:::${AnalyticsBucket}/*"
          - Effect: "Allow"
            Action:
              - "glue:*"
              - "datacatalog:*"
            Resource: "*"
          - Action:
              - "sts:AssumeRole"
            Effect: "Allow"
            Resource: !Ref GlueManagementRoleARN
          - Action:
              - "iam:PassRole"
            Effect: "Allow"
            Resource: !Ref GlueCrawlerRoleARN
  CurDataTransform:
    Type: "AWS::Lambda::Function"
    Properties:
      FunctionName: "BillingAnalytics-CURDataTransform"
      Handler: "cur_data_transform.handler"
      MemorySize: !Ref LambdaMemory
      Role: !Ref LambdaRoleARN
      Runtime: "python3.6"
      Timeout: 300
      Code:
        S3Bucket: !Ref SqueegeeS3BucketName
        S3Key: !Sub "${SqueegeeS3KeyPrefix}functions/cur-data.zip"
      Environment:
        Variables:
          CUR_BUCKET: !Ref CURBucket
          CUR_BUCKET_REGION: !Ref CURBucketRegion
          ANALYTICS_BUCKET: !Ref AnalyticsBucket
          ANALYTICS_BUCKET_REGION: !Ref AnalyticsBucketRegion
          S3_KEY_PREFIX: !Ref KeyPrefix
          GLUE_MANAGEMENT_ROLE: !Ref GlueManagementRoleARN
          GLUE_CRAWLER_NAME: !Ref GlueCrawlerName
          GLUE_CRAWLER_REGION: !Ref GlueCrawlerRegion
          GLUE_CRAWLER_ROLE: !Ref GlueCrawlerRoleARN
          GLUE_CRAWLER_PREFIX: !Ref GlueCrawlerPrefix
          DATACATALOG_DB_NAME: !Ref GlueDataCatalogName
  CurDataTransformerPermission: 
    Type: "AWS::Lambda::Permission"
    Condition: DeploySNSTopic
    Properties: 
      FunctionName: !Ref CurDataTransform
      Action: "lambda:InvokeFunction"
      Principal: "sns.amazonaws.com"
      SourceArn: !Ref CURFileSNS
  CURFileSNS:
    Type: "AWS::SNS::Topic"
    Condition: DeploySNSTopic
    Properties: 
      DisplayName: CURNotifications
      Subscription:
        - Endpoint: !GetAtt CurDataTransform.Arn
          Protocol: lambda
      TopicName: CostUsageReportNotifications
  CURFileSNSPolicy:
    Type: "AWS::SNS::TopicPolicy"
    Condition: DeploySNSTopic
    Properties:
      PolicyDocument:
        Version: "2008-10-17"
        Id: "policy_ID"
        Statement:
          - Sid: "__default_statement_ID"
            Effect: "Allow"
            Principal: 
              AWS: "*"
            Action:
              - "SNS:Subscribe"
              - "SNS:ListSubscriptionsByTopic"
              - "SNS:DeleteTopic"
              - "SNS:GetTopicAttributes"
              - "SNS:Publish"
              - "SNS:RemovePermission"
              - "SNS:AddPermission"
              - "SNS:Receive"
              - "SNS:SetTopicAttributes"
            Resource: !Ref CURFileSNS
            Condition:
              StringEquals:
                "AWS:SourceOwner": "185806833360"
          - Sid: "__CURBucketPublish"
            Effect: "Allow"
            Principal: 
              AWS: "*"
            Action:
              - "SNS:Publish"
            Resource: !Ref CURFileSNS
            Condition:
              ArnLike:
                "aws:SourceArn": !Sub "arn:aws:s3:::${CURBucket}"
      Topics:
        - !Ref CURFileSNS

Outputs: {}
