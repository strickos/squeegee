#!/bin/bash
set -e

for requirements in $(find . -name requirements.txt)
do
  folder=$(dirname ${requirements})
  mkdir -p "${folder}/vendored"
  touch "${folder}/vendored/__init__.py"
  pip3 install -r ${requirements} -t "${folder}/vendored"
done

for folder in $(find . -type d -name scripts)
do
  cd ${folder}
  package=$(echo ${folder} | awk -F'/' '{ print $2 }')
  zip -r ../../deployment/functions/${package}.zip .
  cd -
done

for template in $(find . -type f -name stack.yaml)
do
  echo "Found Template: ${template}"
  package=$(echo ${template} | awk -F'/' '{ print $2 }')
  echo "Generated package name: ${package}"
  cp ${template} ./deployment/templates/${package}.yaml
done
