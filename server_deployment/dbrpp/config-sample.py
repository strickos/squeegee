import json
import logging
import os
import sys
import yaml
import requests
from multiprocessing import cpu_count

logger = logging.getLogger(__name__)


class Config:
    """
    Singleton to store app configuration.
    Generative settings class, similar to the way that Django does settings
    """

    def __init__(self):
        self.app_name = os.getenv('APP_NAME', 'dbrpp')
        self.port = os.getenv('APP_PORT', '8080')
        self.address = os.getenv('APP_ADDRESS', '0.0.0.0')

        # Default to local development
        self.ENV = os.getenv('ENVIRONMENT', 'local')
        self.local = self.ENV == 'local'
        self.DEBUG = True if self.local else False

        self.gunicorn_options = {
            'bind': '%s:%s' % (self.address, self.port),
            'workers': 4,  # TODO: dynamically set
            'worker_class': 'gunicorn.workers.ggevent.GeventWorker',
            'timeout': 60 * 5,
            'logger_class': 'dbrpp.server.Logger',
            'max_requests': 1000
        }
        self.proc_count = 1  # Fixed at one to ensure only one billing file being worked on at a time
        self.WORKER_PIDS = []

        self.VERSION_FILENAME = os.getenv('VERSION_FILENAME', '/usr/src/app/release-version.json')
        ## Version file format:
        # {
        #   "version": "0.0.1"
        # }
        self.release_version = None
        self.version = self.parse_version_json()

        self.master_payer_account_id = os.getenv('MASTER_PAYER_ACCOUNT_ID', '12345678901')
        self.analytics_data_account_id = os.getenv('ANALYTICS_ACCOUNT_ID', '10987654321')

        self.split_line_numbers = os.getenv('SPLIT_LINE_COUNT', '1000000')
        self.split_processing_workers = 7  # Should be one less than cores. TODO: dynamically set

        self.sqs_wait_timeout = 20  # Long polling to reduce costs
        self.sqs_max_number_of_messages = 1
        self.sqs_visibility_timeout = 14400  # Hold message for 4 hours while processing
        self.sqs_queue_url = os.getenv('SQS_DBRPP_NOTIFICATION_QUEUE_URL')
        self.sqs_queue_region = os.getenv('SQS_DBRPP_NOTIFICATION_QUEUE_REGION', 'us-east-1')

        self.temp_dir = os.getenv('WORK_DIR', '/tmp')

        self.billing_files_output_bucket = os.getenv('OUTPUT_FILES_BUCKET', 'billing_files_output_bucket')

        self.analytics_files_output_bucket = os.getenv('ANALYTICS_FILES_BUCKET', 'analytics_files_output_bucket')

        self.configuration_bucket = os.getenv('CONFIGURATION_BUCKET', 'configuration_bucket')

        self.s3_bucket_region = os.getenv('S3_BUCKET_REGION', 'us-east-1')

        self.accounts_file = os.getenv('ACCOUNTS_FILE', 's3://bucket/accounts_file.json')
        self.business_unit_mapping_file = os.getenv('MAPPINGS_FILE', 's3://bucket/mappings.csv')

        self.billing_files_role_arn = os.getenv('BILLING_FILES_ROLE_ARN',
                                                'arn:aws:iam::{}:role/dbr-pp-billing-files-role'.format(self.master_payer_account_id))  # noqa
        self.analytics_files_role_arn = os.getenv('ANALYTICS_FILES_ROLE_ARN',
                                                  'arn:aws:iam::{}:role/dbr-pp-role'.format(self.analytics_data_account_id))  # noqa

        self.billing_file_column_mapping = {
            'cur': {
                'account_id': 'lineItem/UsageAccountId',
                'business_unit': 'resourceTags/user:business_unit',
                'service_cost_group': 'resourceTags/user:service_cost_group',
                'service_id': 'resourceTags/user:micros_service_id',
                'service_name': 'resourceTags/user:service_name'
            },
            'dbr': {
                'account_id': 'LinkedAccountId',
                'business_unit': 'user:business_unit',
                'service_cost_group': 'user:service_cost_group',
                'service_id': 'user:micros_service_id',
                'service_name': 'user:service_name'
            }
        }
        self.cur_selected_columns_manifest = [
            {
                "category": "identity",
                "name": "LineItemId"
            }, {
                "category": "identity",
                "name": "TimeInterval"
            }, {
                "category": "bill",
                "name": "InvoiceId"
            }, {
                "category": "bill",
                "name": "BillingEntity"
            }, {
                "category": "bill",
                "name": "BillType"
            }, {
                "category": "bill",
                "name": "PayerAccountId"
            }, {
                "category": "bill",
                "name": "BillingPeriodStartDate"
            }, {
                "category": "bill",
                "name": "BillingPeriodEndDate"
            }, {
                "category": "lineItem",
                "name": "UsageAccountId"
            }, {
                "category": "lineItem",
                "name": "LineItemType"
            }, {
                "category": "lineItem",
                "name": "UsageStartDate"
            }, {
                "category": "lineItem",
                "name": "UsageEndDate"
            }, {
                "category": "lineItem",
                "name": "ProductCode"
            }, {
                "category": "lineItem",
                "name": "UsageType"
            }, {
                "category": "lineItem",
                "name": "Operation"
            }, {
                "category": "lineItem",
                "name": "AvailabilityZone"
            }, {
                "category": "lineItem",
                "name": "ResourceId"
            }, {
                "category": "lineItem",
                "name": "UsageAmount"
            }, {
                "category": "lineItem",
                "name": "CurrencyCode"
            }, {
                "category": "lineItem",
                "name": "UnblendedRate"
            }, {
                "category": "lineItem",
                "name": "UnblendedCost"
            }, {
                "category": "lineItem",
                "name": "BlendedRate"
            }, {
                "category": "lineItem",
                "name": "BlendedCost"
            }, {
                "category": "lineItem",
                "name": "LineItemDescription"
            }, {
                "category": "lineItem",
                "name": "TaxType"
            }, {
                "category": "product",
                "name": "ProductName"
            }, {
                "category": "product",
                "name": "availability"
            }, {
                "category": "product",
                "name": "availabilityZone"
            }, {
                "category": "product",
                "name": "bundle"
            }, {
                "category": "product",
                "name": "cacheEngine"
            }, {
                "category": "product",
                "name": "cacheMemorySizeGb"
            }, {
                "category": "product",
                "name": "clockSpeed"
            }, {
                "category": "product",
                "name": "comments"
            }, {
                "category": "product",
                "name": "contentType"
            }, {
                "category": "product",
                "name": "currentGeneration"
            }, {
                "category": "product",
                "name": "databaseEdition"
            }, {
                "category": "product",
                "name": "databaseEngine"
            }, {
                "category": "product",
                "name": "dedicatedEbsThroughput"
            }, {
                "category": "product",
                "name": "deploymentOption"
            }, {
                "category": "product",
                "name": "description"
            }, {
                "category": "product",
                "name": "deviceOs"
            }, {
                "category": "product",
                "name": "directConnectLocation"
            }, {
                "category": "product",
                "name": "durability"
            }, {
                "category": "product",
                "name": "ebsOptimized"
            }, {
                "category": "product",
                "name": "ecu"
            }, {
                "category": "product",
                "name": "edition"
            }, {
                "category": "product",
                "name": "endpointType"
            }, {
                "category": "product",
                "name": "engineCode"
            }, {
                "category": "product",
                "name": "enhancedNetworkingSupported"
            }, {
                "category": "product",
                "name": "executionFrequency"
            }, {
                "category": "product",
                "name": "executionLocation"
            }, {
                "category": "product",
                "name": "executionMode"
            }, {
                "category": "product",
                "name": "feeCode"
            }, {
                "category": "product",
                "name": "feeDescription"
            }, {
                "category": "product",
                "name": "freeQueryTypes"
            }, {
                "category": "product",
                "name": "freeTrial"
            }, {
                "category": "product",
                "name": "frequencyMode"
            }, {
                "category": "product",
                "name": "fromLocation"
            }, {
                "category": "product",
                "name": "fromLocationType"
            }, {
                "category": "product",
                "name": "group"
            }, {
                "category": "product",
                "name": "groupDescription"
            }, {
                "category": "product",
                "name": "instanceCapacity10xlarge"
            }, {
                "category": "product",
                "name": "instanceCapacity2xlarge"
            }, {
                "category": "product",
                "name": "instanceCapacity4xlarge"
            }, {
                "category": "product",
                "name": "instanceCapacityLarge"
            }, {
                "category": "product",
                "name": "instanceCapacityXlarge"
            }, {
                "category": "product",
                "name": "instanceFamily"
            }, {
                "category": "product",
                "name": "instanceType"
            }, {
                "category": "product",
                "name": "io"
            }, {
                "category": "product",
                "name": "license"
            }, {
                "category": "product",
                "name": "licenseModel"
            }, {
                "category": "product",
                "name": "location"
            }, {
                "category": "product",
                "name": "locationType"
            }, {
                "category": "product",
                "name": "maxIopsBurstPerformance"
            }, {
                "category": "product",
                "name": "maxIopsvolume"
            }, {
                "category": "product",
                "name": "maxThroughputvolume"
            }, {
                "category": "product",
                "name": "maxVolumeSize"
            }, {
                "category": "product",
                "name": "maximumCapacity"
            }, {
                "category": "product",
                "name": "maximumExtendedStorage"
            }, {
                "category": "product",
                "name": "maximumStorageVolume"
            }, {
                "category": "product",
                "name": "memory"
            }, {
                "category": "product",
                "name": "memoryGib"
            }, {
                "category": "product",
                "name": "messageDeliveryFrequency"
            }, {
                "category": "product",
                "name": "messageDeliveryOrder"
            }, {
                "category": "product",
                "name": "meterMode"
            }, {
                "category": "product",
                "name": "minVolumeSize"
            }, {
                "category": "product",
                "name": "minimumStorageVolume"
            }, {
                "category": "product",
                "name": "networkPerformance"
            }, {
                "category": "product",
                "name": "operatingSystem"
            }, {
                "category": "product",
                "name": "operation"
            }, {
                "category": "product",
                "name": "origin"
            }, {
                "category": "product",
                "name": "physicalCores"
            }, {
                "category": "product",
                "name": "physicalProcessor"
            }, {
                "category": "product",
                "name": "portSpeed"
            }, {
                "category": "product",
                "name": "preInstalledSw"
            }, {
                "category": "product",
                "name": "processorArchitecture"
            }, {
                "category": "product",
                "name": "processorFeatures"
            }, {
                "category": "product",
                "name": "productFamily"
            }, {
                "category": "product",
                "name": "provisioned"
            }, {
                "category": "product",
                "name": "queueType"
            }, {
                "category": "product",
                "name": "recipient"
            }, {
                "category": "product",
                "name": "requestDescription"
            }, {
                "category": "product",
                "name": "requestType"
            }, {
                "category": "product",
                "name": "resourceEndpoint"
            }, {
                "category": "product",
                "name": "resourceType"
            }, {
                "category": "product",
                "name": "routingTarget"
            }, {
                "category": "product",
                "name": "routingType"
            }, {
                "category": "product",
                "name": "runningMode"
            }, {
                "category": "product",
                "name": "servicecode"
            }, {
                "category": "product",
                "name": "sku"
            }, {
                "category": "product",
                "name": "sockets"
            }, {
                "category": "product",
                "name": "softwareIncluded"
            }, {
                "category": "product",
                "name": "softwareType"
            }, {
                "category": "product",
                "name": "standardStorageRetentionIncluded"
            }, {
                "category": "product",
                "name": "storage"
            }, {
                "category": "product",
                "name": "storageClass"
            }, {
                "category": "product",
                "name": "storageDescription"
            }, {
                "category": "product",
                "name": "storageMedia"
            }, {
                "category": "product",
                "name": "storageType"
            }, {
                "category": "product",
                "name": "subscriptionType"
            }, {
                "category": "product",
                "name": "tenancy"
            }, {
                "category": "product",
                "name": "toLocation"
            }, {
                "category": "product",
                "name": "toLocationType"
            }, {
                "category": "product",
                "name": "transferType"
            }, {
                "category": "product",
                "name": "trialProduct"
            }, {
                "category": "product",
                "name": "upfrontCommitment"
            }, {
                "category": "product",
                "name": "usageFamily"
            }, {
                "category": "product",
                "name": "usagetype"
            }, {
                "category": "product",
                "name": "vcpu"
            }, {
                "category": "product",
                "name": "version"
            }, {
                "category": "product",
                "name": "virtualInterfaceType"
            }, {
                "category": "product",
                "name": "volumeType"
            }, {
                "category": "product",
                "name": "withActiveUsers"
            }, {
                "category": "pricing",
                "name": "LeaseContractLength"
            }, {
                "category": "pricing",
                "name": "OfferingClass"
            }, {
                "category": "pricing",
                "name": "PurchaseOption"
            }, {
                "category": "pricing",
                "name": "publicOnDemandCost"
            }, {
                "category": "pricing",
                "name": "publicOnDemandRate"
            }, {
                "category": "pricing",
                "name": "term"
            }, {
                "category": "pricing",
                "name": "unit"
            }, {
                "category": "reservation",
                "name": "AvailabilityZone"
            }, {
                "category": "reservation",
                "name": "NumberOfReservations"
            }, {
                "category": "reservation",
                "name": "ReservationARN"
            }, {
                "category": "reservation",
                "name": "TotalReservedUnits"
            }, {
                "category": "reservation",
                "name": "UnitsPerReservation"
            }, {
                "name": "user:Name",
                "category": "resourceTags"
            }, {
                "name": "user:business_unit",
                "category": "resourceTags"
            }, {
                "name": "user:environment",
                "category": "resourceTags"
            }, {
                "name": "user:resource_owner",
                "category": "resourceTags"
            }, {
                "name": "user:service_name",
                "category": "resourceTags"
            }, {
                "name": "user:cost_allocation",
                "category": "resourceTags"
            }, {
                "name": "user:service_cost_group",
                "category": "resourceTags"
            }
        ]
        self.cur_selected_columns = ['{}/{}'.format(col['category'],
                                                    col['name']) for col in self.cur_selected_columns_manifest]
        self.dbr_selected_columns = ['InvoiceID',
                                     'PayerAccountId',
                                     'LinkedAccountId',
                                     'RecordType',
                                     'RecordId',
                                     'ProductName',
                                     'RateId',
                                     'SubscriptionId',
                                     'PricingPlanId',
                                     'UsageType',
                                     'Operation',
                                     'AvailabilityZone',
                                     'ReservedInstance',
                                     'ItemDescription',
                                     'UsageStartDate',
                                     'UsageEndDate',
                                     'UsageQuantity',
                                     'BlendedRate',
                                     'BlendedCost',
                                     'UnBlendedRate',
                                     'UnBlendedCost',
                                     'ResourceId',
                                     'user:Name',
                                     'user:business_unit',
                                     'user:environment',
                                     'user:resource_owner',
                                     'user:service_name',
                                     'user:cost_allocation',
                                     'user:service_cost_group']

        self.logger = logging.getLogger(self.app_name)
        self.logger.info('Initialized logging')
        logging_file = 'logging.yaml' if not self.DEBUG else 'logging.debug.yaml'
        with open(logging_file, 'rt') as f:
            self.logging_config = yaml.safe_load(f.read())

    def parse_version_json(self):
        """
        Parse the release-version.json files and store
        the results.
        """
        if self.local:
            return 'local'
        try:
            with open(self.VERSION_FILENAME, 'r') as f:
                self.release_version = json.load(f)
        except FileNotFoundError:
            self.logger.error('Unable to read version config: %s', self.VERSION_FILENAME)
            self.release_version = 'no-config'
            return self.release_version

        return self.release_version['version']

    def version_info(self):
        info = {
            'version': self.version,
            'service_name': self.app_name,
            'python_version': sys.version.replace('\n', '')
        }
        return info

    def get_instance_id(self):
        if self.local:
            return 'local'
        try:
            instance_id = requests.get('http://169.254.169.254/latest/meta-data/instance-id').text
            self.logger.debug('Setting instance_id to: %s', instance_id)
            return instance_id
        except:
            self.logger.exception('Unable to get instance id from metadata')
            raise

    def process_row(self, row, business_unit_mappings, default_business_unit_mappings, file_type):
        """Process billing file row and return the data to go in the output file

        :param row: billing row to be processed
        :param business_unit_mappings: The mapping from bad BU names to good
        :param default_business_unit_mappings: The mapping from account id to default BU
        :param file_type: The type of billing file this row is from (eg. cur or dbr)
        :type row: dict
        :type business_unit_mappings: dict
        :type default_business_unit_mappings: dict
        :type file_type: str
        :returns: list
        """

        if file_type == 'dbr':
            new_header = self.dbr_selected_columns
            ca_tag = 'user:cost_allocation'
            usage_start_date_tag = 'UsageStartDate'
            usage_end_date_tag = 'UsageEndDate'
        elif file_type == 'cur':
            new_header = self.cur_selected_columns
            ca_tag = 'resourceTags/user:cost_allocation'
            usage_start_date_tag = 'lineItem/UsageStartDate'
            usage_end_date_tag = 'lineItem/UsageEndDate'
        else:
            self.logger.warning('unsupported file type: %s', file_type)
            return None

        if not business_unit_mappings:
            business_unit_mappings = {}
        if not default_business_unit_mappings:
            default_business_unit_mappings = {}

        account_id_tag_name = self.billing_file_column_mapping[file_type].get('account_id', '')
        account_id = row.get(account_id_tag_name, False)
        business_unit_tag_name = self.billing_file_column_mapping[file_type].get('business_unit', '')
        cost_allocation = row.get(business_unit_tag_name, False)

        if not cost_allocation:
            if account_id not in default_business_unit_mappings:
                cost_allocation = 'Mapping Missing'
            else:
                cost_allocation = default_business_unit_mappings.get(account_id, '')
        row[ca_tag] = cost_allocation

        # Business Unit Mapping
        if row[ca_tag].lower() in business_unit_mappings:
            row[ca_tag] = business_unit_mappings[row[ca_tag].lower()]

        usage_start_date = row[usage_start_date_tag] if row[usage_start_date_tag] != '' else None
        usage_end_date = row[usage_end_date_tag] if row[usage_start_date_tag] != '' else None

        # Strip leading and trailing whitespace from each value.
        for name, value in row.items():
            row[name] = value.strip()

        return usage_start_date, usage_end_date, [row.get(h, '') for h in new_header]


config = Config()
