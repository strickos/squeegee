from ..config import config
from botocore.exceptions import ClientError
from datetime import date, datetime
from json import JSONDecodeError
from multiprocessing import Pool
from subprocess import Popen, PIPE
from tempfile import mkdtemp
import csv
import boto3
import json
import logging
import os
import shutil

logger = logging.getLogger(__name__)


def change_message_visibility(queue_url, receipt_handle, timeout=300):
    """Change SQS message visibility.

    :param queue_url: Queue message resides on.
    :param receipt_handle: Receipt handle for message.
    :param timeout: Message will be put on back queue after this timeout.
    :type queue_url: str
    :type receipt_handle: str
    :type timeout: int
    :returns: None
    """
    client = get_client(service='sqs', region_name=config.sqs_queue_region)

    client.change_message_visibility(QueueUrl=queue_url,
                                     ReceiptHandle=receipt_handle,
                                     VisibilityTimeout=timeout)


def compress_file(filename, identity, compression='gzip'):
    logging.info("{identity}: Started compress of file: {filename}".format(identity=identity,
                                                                           filename=filename))
    if compression == 'gzip':
        ps = Popen(("/bin/gzip", filename))
        ps.wait()
    else:
        logging.error("{identity}: Unknown compression type: {compression} for uncompress file".format(identity=identity,  # noqa
                                                                                                       compression=compression))  # noqa
        raise Exception('UnknownCompressionType')
    logging.info("{identity}: Finished compress of file: {filename}".format(identity=identity,
                                                                            filename=filename))


def split_s3_url_into_parts(url):
    """Split an S3 url (s3://bucket/path/filename.txt) into its individual components.

    :param url: S3 URL.
    :type url: str
    :returns str, str, str
    """
    bucket, key = url.split('//')[1].split('/', maxsplit=1)
    path = key.split('/')[:-1]
    filename = key.split('/')[-1]

    return bucket, path, filename


def copy_file_to_monthly_directory(current_date, identity, source):
    """Copy file to the monthly dated output directory.

    :param current_date: Current date in YYYY-MM format.
    :param identity: x-amz-request-id used to identify the record being processed.
    :param source: File in S3 in the format s3://bucket/directory/filename.
    :type current_date: str
    :type identity: str
    :type source: str
    :returns: bool
    """

    client = get_client(service='s3',
                        arn=config.analytics_files_role_arn,
                        region_name=config.s3_bucket_region)

    bucket, path, filename = split_s3_url_into_parts(source)
    destination_key = '/'.join(path + ['monthly', current_date, filename])

    copy_source = {
        'Bucket': bucket,
        'Key': '/'.join(source.split('//')[1].split('/')[1:])
    }

    logging.info("{identity}: Attempting to copy {source} to s3://{bucket}/{destination_filename}.".format(identity=identity,  # noqa
                                                                                                           bucket=bucket,  # noqa
                                                                                                           destination_filename=destination_key,  # noqa
                                                                                                           source=source))  # noqa

    client.copy_object(ACL='private',
                       Bucket=bucket,
                       Key=destination_key,
                       CopySource=copy_source)

    logging.info("{identity}: Successfully copied {source} to s3://{bucket}/{destination_filename}.".format(identity=identity,  # noqa
                                                                                                            bucket=bucket,  # noqa
                                                                                                            destination_filename=destination_key,  # noqa
                                                                                                            source=source))  # noqa

    return True


def copy_cost_allocation_file(current_date, bucket_name, identity):
    """Copy the Cost Allocation File to output directory.

    :param current_date: Current date in YYYY-MM format.
    :param bucket_name: Name of the S3 bucket to look for CA file in
    :param identity: x-amz-request-id used to identify the record being processed.
    :type current_date: str
    :type bucket_name: str
    :type identity: str
    :returns: None
    """
    client = get_client(service='s3', arn=config.billing_files_role_arn)

    # TODO: Review behaviour on 1st day of month.

    caf_file_name = get_latest_cost_allocation_file(current_date, bucket_name, identity)
    if not caf_file_name:
        return

    copy_source = {
        'Bucket': bucket_name,
        'Key': caf_file_name
    }

    logging.info(
        "{identity}: Started copying s3://{source_bucket}/{caf_file_name} to s3://{output_bucket}/{caf_file_name}.".format(  # noqa
            identity=identity,
            output_bucket=config.billing_files_output_bucket,
            source_bucket=bucket_name,
            caf_file_name=caf_file_name
        ))

    client.copy_object(ACL='private',
                       Bucket=config.billing_files_output_bucket,
                       Key=caf_file_name,
                       CopySource=copy_source)

    logging.info(
        "{identity}: Finished copying s3://{source_bucket}/{caf_file_name} to s3://{output_bucket}/{caf_file_name}.".format(  # noqa
            identity=identity,
            output_bucket=config.billing_files_output_bucket,
            source_bucket=bucket_name,
            caf_file_name=caf_file_name
        ))


def current_year_month():
    """Returns the current year and month as a string in format YYYY-MM.

    :returns: bool
    """
    return date.today().strftime('%Y-%m')


def decode_json(json_data):
    """JSON Decode the S3 Put message.

    :param json_data: JSON Data.
    :type: str
    :returns: decoded JSON
    :rtype: dict
    """
    try:
        result_dict = json.loads(json_data)
    except JSONDecodeError:
        logger.error("Data cannot be decoded as JSON: %s", json_data)
        return None
    return result_dict


def delete_message(message_obj):
    """Delete SQS message from the queue.

    :param message_obj: SQS message.
    :type message_obj: message object
    :returns: True if message deleted.
    :rtype: bool"""
    client = get_client(service='sqs', region_name=config.sqs_queue_region)
    logger.info('Deleting message {receipt_handle} from {queue}'.format(receipt_handle=message_obj.receipt_handle,
                                                                        queue=message_obj.queue_url))
    client.delete_message(
        QueueUrl=message_obj.queue_url,
        ReceiptHandle=message_obj.receipt_handle
    )
    return True


def delete_temp_directories(identity, temp_dir=config.temp_dir, full_cleanup=False):
    """Delete the temporary working directories.

    :param identity: x-amz-request-id for the record.
    :param temp_dir: Temp directories will be created in dir.
    :param full_cleanup: If set will clean out the whole temp dir.
    :type identity: str
    :type temp_dir: str
    :type full_cleanup: bool
    :returns: None
    """
    # If identity is None then clear out entire temporary scratch path.
    if full_cleanup:
        temp_path = "{0}".format(temp_dir)
        logging.info("Started deleting temporary path: {path}".format(identity=identity, path=temp_path))
        for root, dirs, files in os.walk(temp_path):
            for directory in dirs:
                if os.path.isdir("{0}/{1}".format(temp_path, directory)):
                    if directory != 'lost+found':
                        shutil.rmtree("{0}/{1}".format(temp_path, directory))
        logging.info("Finished deleting temporary path: {path}.".format(identity=identity, path=temp_path))

    else:
        temp_path = "{0}/{1}".format(temp_dir, identity)
        logging.info("{identity}: Started deleting temporary path: {path}".format(identity=identity, path=temp_path))
        shutil.rmtree(temp_path)
        logging.info("{identity}: Finished deleting temporary path: {path}.".format(identity=identity, path=temp_path))


def download_s3_file_to_local(bucket, key, local_filename, identity, region='us-east-1'):
    """Download an S3 object to local filesystem

    :param bucket: Name of the S3 Bucket
    :param key: Name of the S3 key
    :param local_filename: Local filename to download to
    :param identity: x-amz-request-id used to identify the record being processed.
    :param region: Region to use for client connection
    :type bucket: str
    :type key: str
    :type local_filename: str
    :type identity: str
    :type region: str
    :returns: Copy response
    :rtype: dict
    """
    logging.info("{identity}: Started download for file s3://{source_bucket}/{source_key} to {local_filename}".format(identity=identity,  # noqa
                                                                                                                      source_bucket=bucket,  # noqa
                                                                                                                      source_key=key,  # noqa
                                                                                                                      local_filename=local_filename))  # noqa
    client = get_client(service='s3', arn=config.billing_files_role_arn, region_name=region)
    response = client.download_file(bucket, key, local_filename)
    logging.info("{identity}: Finished downloading file s3://{source_bucket}/{source_key} to {local_filename}".format(identity=identity,  # noqa
                                                                                                                source_bucket=bucket,  # noqa
                                                                                                                source_key=key,  # noqa
                                                                                                                local_filename=local_filename))  # noqa

    return response


def download_s3_files_to_local(bucket, keys, local_files_dir, identity, region='us-east-1'):
    """Download multiple S3 objects to local filesystem

    :param bucket: Name of the S3 Bucket
    :param keys: Name of the S3 keys
    :param local_files_dir: Local directory to download to
    :param identity: x-amz-request-id used to identify the record being processed.
    :param region: Region to use for client connection
    :type bucket: str
    :type keys: list
    :type local_files_dir: str
    :type identity: str
    :type region: str
    :returns: None
    """
    for key in keys:
        local_filename = '{}/{}'.format(local_files_dir, key.split('/')[-1])
        download_s3_file_to_local(bucket,
                                  key,
                                  local_filename,
                                  identity,
                                  region)


def join_files(filename_list, output_file, identity, compression=None):
    """Split file by lines.

    :param filename_list: List of file names to join.
    :param output_file: name of the file
    :param identity: x-amz-request-id used to identify the record being processed.
    :param compression: The compression type of the file
    :type filename_list: list
    :type identity: str
    :type compression: str
    :returns: None
    """
    logging.info("{identity}: Started join of {filename_list} to {output_file}".format(identity=identity,
                                                                                       filename_list=filename_list,
                                                                                       output_file=output_file))
    Popen(("/bin/cat", *filename_list), stdout=output_file)
    if compression == 'gzip':
        Popen(("/bin/gzip", output_file))
    elif compression == 'zip':
        Popen(("/usr/bin/zip", "{}.zip".format(output_file), output_file))
    logging.info("{identity}: Finished join of {filename_list} to {output_file}".format(identity=identity,
                                                                                        filename_list=filename_list,
                                                                                        output_file=output_file))


def get_client(service='s3', arn=None, region_name='us-east-1', session_name='dbr_pipeline'):
    """Return a Boto3 client connection.

    :param service: AWS service to generate a client connection for.
    :param arn: ARN of Role to assume.
    :param region_name: Name of the region to connect to
    :param session_name: AWS session name to use.
    :type service: str
    :type arn: str
    :type region_name: str
    :type session_name: str
    :returns: client object
    """
    sts_client = boto3.client('sts', region_name=region_name)
    if arn is None:
        client = boto3.client(service, region_name=region_name)
    else:
        credentials = sts_client.assume_role(RoleArn=arn,
                                             RoleSessionName=session_name)['Credentials']

        client = boto3.client(service,
                              region_name=region_name,
                              aws_access_key_id=credentials['AccessKeyId'],
                              aws_secret_access_key=credentials['SecretAccessKey'],
                              aws_session_token=credentials['SessionToken'])

    return client


def get_latest_cost_allocation_file(current_date, bucket_name, identity):
    """Return bucket and path the latest cost allocation file.

    :param current_date: Current date in YYYY-MM format.
    :param bucket_name: Name of S3 bucket to find CA file in
    :param identity: x-amz-request-id used to identify the record being processed.
    :type current_date: str
    :type bucket_name: str
    :type identity: str
    :returns: str
    """
    client = get_client(service='s3', arn=config.billing_files_role_arn)
    filename = "{0}-aws-cost-allocation-{1}.csv".format(config.master_payer_account_id, current_date)

    logging.info("{identity}: Looking for s3://{bucket}/{filename}".format(identity=identity, bucket=bucket_name, filename=filename))  # noqa
    try:
        client.head_object(
            Bucket=bucket_name,
            Key=filename,
        )
    except ClientError:
        logging.warning("{identity}: Failed to find s3://{bucket}/{filename}".format(identity=identity, bucket=bucket_name, filename=filename))  # noqa
        return None
    logging.info("{identity}: Found s3://{bucket}/{filename}".format(identity=identity, bucket=bucket_name, filename=filename))  # noqa

    return filename


def get_resource(service='s3', arn=None, region_name='us-east-1', session_name='dbr_pipeline'):
    """Return a Boto3 resource connection.

    :param service: AWS service to generate a resource for.
    :param arn: ARN of Role to assume.
    :param region_name: Name of region to use
    :param session_name: AWS session name to use.
    :type service: str
    :type arn: str
    :type region_name: str
    :type session_name: str
    :returns: resource object
    """
    client = boto3.client('sts', region_name=region_name)
    if arn is None:
        resource = boto3.resource(service, region_name=region_name)
    else:
        credentials = client.assume_role(RoleArn=arn,
                                         RoleSessionName=session_name)['Credentials']

        resource = boto3.resource(service,
                                  region_name=region_name,
                                  aws_access_key_id=credentials['AccessKeyId'],
                                  aws_secret_access_key=credentials['SecretAccessKey'],
                                  aws_session_token=credentials['SessionToken'])

    return resource


def load_business_unit_mappings(record_date, identity):
    """Download the business unit mapping file from S3 and return it as dict.

    :param record_date: Date from the SQS message record.
    :param identity: x-amz-request-id used to identify the record being processed.
    :type record_date: str
    :type identity: str
    :returns: Invalid to Valid business unit mappings.
    :rtype: dict
    """
    download_path = make_temp_dir(identity)
    bucket, path, filename = split_s3_url_into_parts(config.business_unit_mapping_file)
    destination_filename = "{0}/{1}".format(download_path, filename)
    s3_object = '/'.join(path + ['monthly', record_date, filename])

    client = get_client(service='s3', arn=config.analytics_files_role_arn)

    logging.info("{identity}: Attempting to download business_unit mapping file s3://{source_bucket}/{source_object} to {destination}".format(identity=identity,  # noqa
                                                                                                                                              source_bucket=bucket,  # noqa
                                                                                                                                              source_object=s3_object,  # noqa
                                                                                                                                              destination=destination_filename))  # noqa
    client.download_file(bucket, s3_object, destination_filename)
    logging.info("{identity}: Finished downloading business_unit mapping file s3://{source_bucket}/{source_object} to {destination}".format(identity=identity,  # noqa
                                                                                                                                            source_bucket=bucket,  # noqa
                                                                                                                                            source_object=s3_object,  # noqa
                                                                                                                                            destination=destination_filename))  # noqa

    with open(destination_filename) as bad_bu_mapping_file_handle:
        reader = csv.DictReader(bad_bu_mapping_file_handle)
        mappings = {r['business_unit'].lower(): r['Department'] for r in reader}

    shutil.rmtree(download_path)

    return mappings


def load_default_business_unit_mappings(record_date, identity):
    """Download default business unit mappings from S3 and return them as dict.

    :param record_date: Date from the SQS message record.
    :param identity: x-amz-request-id used to identify the record being processed.
    :type record_date: str
    :type identity: str
    :returns: Default business unit mappings.
    :rtype: dict
    """
    download_path = make_temp_dir(identity)
    bucket, path, filename = split_s3_url_into_parts(config.accounts_file)
    destination_filename = "{0}/{1}".format(download_path, filename)
    s3_object = '/'.join(path + ['monthly', record_date, filename])

    client = get_client(service='s3', arn=config.analytics_files_role_arn)

    logging.info("{identity}: Attempting to download default business_unit mappings file file s3://{source_bucket}/{s3_object} to {destination}".format(identity=identity,  # noqa
                                                                                                                                                        source_bucket=bucket,  # noqa
                                                                                                                                                        s3_object=s3_object,  # noqa
                                                                                                                                                        destination=destination_filename))  # noqa
    client.download_file(bucket, s3_object, destination_filename)
    logging.info("{identity}: Finished downloading default business_unit mappings file s3://{source_bucket}/{s3_object} to {destination}".format(identity=identity,  # noqa
                                                                                                                                                 source_bucket=bucket,  # noqa
                                                                                                                                                 s3_object=s3_object,  # noqa
                                                                                                                                                 destination=destination_filename))  # noqa

    with open(destination_filename) as default_business_unit_mappings_file_handle:
        reader = csv.DictReader(default_business_unit_mappings_file_handle)
        mappings = {r['account_id']: r['default_business_unit'] for r in reader}

    shutil.rmtree(download_path)

    return mappings


def make_temp_dir(identity, suffix='', prefix='tmp', temp_dir=config.temp_dir):
    """Create a temp directory and return its path.

    :param identity: x-amz-request-id used to identify the record being processed.
    :param suffix: Directory names will end with suffix.
    :param prefix: Directory names will start with prefix.
    :param temp_dir: Temp directories will be created in dir.
    :type identity: str
    :type suffix: str
    :type prefix: str
    :type temp_dir: str
    :returns: Temporary directory path.
    :rtype: str
    """
    temp_path = "{0}/{1}/".format(temp_dir, identity)
    logger.info('{identity}: Started creating temp directory'.format(identity=identity))
    if not os.path.exists(temp_path):
        os.makedirs(temp_path)
    temp_dir_name = mkdtemp(suffix=suffix, prefix=prefix, dir=temp_path)
    logger.info('{identity}: Finished creating temp directory: {temp_dir_name}'.format(identity=identity,
                                                                                       temp_dir_name=temp_dir_name))
    return temp_dir_name


def process_splits(file_splits_path, header, business_unit_mappings, default_business_unit_mappings, identity, file_type, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL):  # noqa
    """Process billing file row and return the data to go in the output file

    :param file_splits_path: path to the input split files
    :param header: list of headers for input billing file
    :param business_unit_mappings: The mapping from bad BU names to good
    :param default_business_unit_mappings: The mapping from account id to default BU
    :param identity: x-amz-request-id used to identify the record being processed.
    :param file_type: The type of billing file this row is from (eg. cur or dbr)
    :param delimiter: The delimiter to use when writing out the CSV.
    :param quotechar: The quotechar to use when writing out the CSV.
    :param quoting: The constant to use for what to quote when writing out the CSV.
    :type file_splits_path: str
    :type header: list
    :type business_unit_mappings: dict
    :type default_business_unit_mappings: dict
    :type identity: str
    :type file_type: str
    :type delimiter: str
    :type quotechar: str
    :type quoting: constant
    :returns: list
    """
    logging.info("{identity}: Started processing of individual splits.".format(identity=identity))
    output_path = make_temp_dir(identity)
    analytics_files_output_path = make_temp_dir(identity)
    pool = Pool(processes=config.split_processing_workers)

    pool.map(process_split_file, [[filename,
                                   file_splits_path,
                                   output_path,
                                   analytics_files_output_path,
                                   header,
                                   business_unit_mappings,
                                   default_business_unit_mappings,
                                   identity,
                                   file_type,
                                   delimiter,
                                   quotechar,
                                   quoting] for filename in os.listdir(file_splits_path)])
    pool.close()
    pool.join()  # Wait for the pool to complete

    logging.info("{identity}: Finished processing of individual splits.".format(identity=identity))
    return output_path, analytics_files_output_path


def process_split_file(arg):
    """Process billing split file and write out processed lines into new file

    :param arg: Array of arguments to send the process_row method
    :type arg: list
    :returns: list
    """
    try:
        filename, input_path, output_path, analytics_files_output_path, header, business_unit_mappings, default_business_unit_mappings, identity, file_type, delimiter, quotechar, quoting = arg  # noqa

        logging.info(
            "{identity}: Started processing split - input: {input_path}/{filename} output: {output_path}/{filename}".format(  # noqa
                input_path=input_path,
                filename=filename,
                output_path=output_path,
                identity=identity))

        with open('{0}/{1}'.format(input_path, filename)) as file_handler:
            reader = csv.DictReader(file_handler, header)
            # Remove original header from datafiles and set timestamp format string.
            if file_type == 'cur':
                next(reader)
                new_header = config.cur_selected_columns
                datetime_format = '%Y-%m-%dT%H:%M:%SZ'
            elif file_type == 'dbr':

                if filename == 'xaa':
                    next(reader)
                new_header = config.dbr_selected_columns

                datetime_format = '%Y-%m-%d %H:%M:%S'
            else:
                logger.warning('{identity}: Unknown file type: {file_type}'.format(identity=identity,
                                                                                   file_type=file_type))
                return False

            # We have 2 csv.writers here as we need to write out a normal comma separated csv for repackaged file
            # and also a ctrl-a separated file without quotes for analytics (Athena).
            with open('{0}/{1}'.format(output_path, filename), 'w', newline="\n") as out_file_handle, \
                    open('{0}/{1}'.format(analytics_files_output_path, filename), 'w', newline="\n") as analytics_out_file_handle:  # noqa
                csv_writer = csv.writer(out_file_handle,
                                        delimiter=delimiter,
                                        quotechar=quotechar,
                                        quoting=quoting)
                csv_writer_analytics = csv.writer(analytics_out_file_handle,
                                                  delimiter=chr(1),
                                                  quotechar='"',
                                                  quoting=csv.QUOTE_NONE)
                if filename == 'xaa' or file_type == 'cur':
                    # Write out header
                    # Header is just used for repackaged file, not the analytics file.
                    csv_writer.writerow(new_header)

                earliest_usage_start_date = None
                latest_usage_end_date = None

                for row in reader:
                    processed_row = config.process_row(row,
                                                       business_unit_mappings,
                                                       default_business_unit_mappings,
                                                       file_type)
                    csv_writer.writerow(processed_row[2])

                    csv_writer_analytics.writerow(processed_row[2])

                    # Skip lines such as Total Statement, etc that don't have usage start or end timestamps.
                    if processed_row[0] is not None and processed_row[1] is not None:
                        tmp_datetime = dict()
                        for var_name, datetimestamp in [('start', processed_row[0]), ('end', processed_row[1])]:
                            try:
                                tmp_datetime[var_name] = datetime.strptime(datetimestamp, datetime_format)
                            except ValueError:
                                logger.exception("{identity}: Date format does not match datetime string: {var_name} - {datetime} in filename: {filename}. Row contents: {row}".format(  # noqa
                                    identity=identity,
                                    datetime=datetimestamp,
                                    filename=filename,
                                    row=processed_row,
                                    var_name=var_name,
                                ))

                        # If temp usage start date is earlier than earliest_usage_start_date, then update.
                        if earliest_usage_start_date is None or tmp_datetime['start'] < earliest_usage_start_date:
                            earliest_usage_start_date = tmp_datetime['start']

                        # If temp usage end date is later than latest_usage_end_date, then update.
                        if latest_usage_end_date is None or tmp_datetime['end'] > latest_usage_end_date:
                            latest_usage_end_date = tmp_datetime['end']

                logger.info("{identity}: Earliest usage_start_date for {input_path}/{filename}: {timestamp}"
                            .format(identity=identity,
                                    timestamp=earliest_usage_start_date,
                                    input_path=input_path,
                                    filename=filename))
                logger.info("{identity}: Latest usage_end_date for {input_path}/{filename}: {timestamp}"
                            .format(identity=identity,
                                    timestamp=latest_usage_end_date,
                                    input_path=input_path,
                                    filename=filename))

        logging.info(
            "{identity}: Finished processing split - input: {input_path}/{filename} output: {output_path}/{filename}".format(  # noqa
                input_path=input_path,
                filename=filename,
                output_path=output_path,
                identity=identity))
    except Exception as e:
        logging.exception("%s", e)
        return False
    return True


def split_file(filename, line_count, output_path, identity, compression='zip'):
    """Split file by lines.

    :param filename: Filename of file to split.
    :param line_count: Number of lines to split on
    :param output_path: path to put the output file
    :param identity: x-amz-request-id used to identify the record being processed.
    :param compression: The compression type of the file
    :type filename: str or list
    :type line_count: int
    :type identity: str
    :type compression: str
    :returns: None
    """
    logging.info("{identity}: Started split of {filename} to {output_path}".format(identity=identity,
                                                                                   filename=filename,
                                                                                   output_path=output_path))
    if isinstance(filename, str):
        filename = [filename]
    try:
        cwd = os.getcwd()
    except FileNotFoundError:
        logging.warning("{identity}: CWD disappeared. Catching error and setting CWD to '/usr/src/app'.")
        cwd = '/usr/src/app'
    os.chdir(output_path)
    if compression == 'zip':
        ps = Popen(("/usr/bin/unzip", "-p", *filename), stdout=PIPE)
    elif compression == 'gzip':
        ps = Popen(("/bin/zcat", *filename), stdout=PIPE)
    else:
        ps = Popen(("/bin/cat", *filename), stdout=PIPE)
    Popen(("/usr/bin/split", "-l", str(line_count), "-"), stdin=ps.stdout)
    ps.wait()
    os.chdir(cwd)
    logging.info("{identity}: Finished split of {filename} to {output_path}".format(identity=identity,
                                                                                    filename=filename,
                                                                                    output_path=output_path))


def uncompress_file(filename, identity, compression='gzip'):
    logging.info("{identity}: Started uncompress of file: {filename}".format(identity=identity,
                                                                             filename=filename))
    if compression == 'gzip':
        ps = Popen(("/bin/gunzip", filename))
        ps.wait()
    else:
        logging.error("{identity}: Unknown compression type: {compression} for uncompress file".format(identity=identity,  # noqa
                                                                                                       compression=compression))  # noqa
        raise Exception('UnknownCompressionType')
    logging.info("{identity}: Finished uncompress of file: {filename}".format(identity=identity,
                                                                              filename=filename))


def upload_local_file_to_s3(bucket, key, local_filename, identity, role_arn=None, region='us-east-1'):
    """Upload a local file to S3

    :param bucket: Name of the S3 Bucket
    :param key: Name of the S3 key
    :param local_filename: Local filename to upload
    :param identity: x-amz-request-id used to identify the record being processed.
    :param role_arn: Role to use when performing the copy
    :param region: Region to use for client connection
    :type bucket: str
    :type key: str
    :type local_filename: str
    :type identity: str
    :type role_arn: str
    :type region: str
    :returns: Copy response(s)
    :rtype: dict
    """
    client = get_client(service='s3', arn=role_arn, region_name=region)

    logging.info(
        "{identity}: Started upload for file {local_filename} to s3://{source_bucket}/{source_key}".format(
            identity=identity,
            source_bucket=bucket,
            source_key=key,
            local_filename=local_filename))

    response = client.upload_file(local_filename, bucket, key)
    logging.info("{identity}: Finished uploading file {local_filename} to s3://{source_bucket}/{source_key}".format(
        identity=identity,
        source_bucket=bucket,
        source_key=key,
        local_filename=local_filename))
    return response


def purger_s3_files_in_dir(bucket, prefix, identity, role_arn=None, region='us-east-1'):
    """purger files in a directory in S3


    :param bucket: Name of the S3 Bucket
    :param prefix: directory name of the S3 bucket
    :param identity: x-amz-request-id used to identify the record being processed.
    :param role_arn: Role to use when performing the copy
    :param region: Region to use for client connection
    :type bucket: str
    :type prefix: str
    :type identity: str
    :type role_arn: str
    :type region: str
    :returns: True if files have been deleted, False if not.
    :rtype: bool
    """
    keys_to_delete = []
    client = get_client(service='s3', arn=role_arn, region_name=region)

    response = client.list_objects(Bucket=bucket, Prefix=prefix)
    if not prefix.endswith('/'):
        prefix += '/'
    if 'Contents' in response:
        for keyset in response['Contents']:
            if keyset['Key'].split(prefix)[1]:
                keys_to_delete.append({'Key': keyset['Key']})
    else:
        logging.info('{identity}: there is no directory {prefix} in S3 {bucket} to clear up'.format(
            identity=identity,
            prefix=prefix,
            bucket=bucket))
    if keys_to_delete:
        purger_response = client.delete_objects(Bucket=bucket, Delete={'Objects': keys_to_delete})
        if 'Errors' in purger_response:
            for error in purger_response['Errors']:
                logging.error('{identity}: cannot delete file {key} in S3 {bucket} with error {message}'.format(
                    identity=identity,
                    key=error['Key'],
                    bucket=bucket,
                    message=error['Message']))
            return False
        else:
            return True
    else:
        return True


def validate_message(message_obj):
    """Validate the format of a S3 Put SQS message.

    :param message_obj: SQS message.
    :type: message object
    :returns: True if message is valid, False if not.
    :rtype: bool
    """
    try:
        # Check if the message body is valid json.
        message = decode_json(message_obj.body)
        if not message:
            logger.warning("Message decoding failed. Skipping.")
        if 'Records' not in message:
            logger.warning('Message missing key "Records". Skipping: %s', message)
            return False
        if not message['Records']:
            logger.warning('Message missing data in "Records". Skipping: %s', message)
            return False
        notification = message['Records'][0]
        if 'eventName' not in notification or\
           not notification['eventName'].startswith('ObjectCreated:') or\
           's3' not in notification or\
           'bucket' not in notification['s3'] or\
           'name' not in notification['s3']['bucket'] or\
           'object' not in notification['s3'] or\
           'key' not in notification['s3']['object'] or\
           'responseElements' not in notification or\
           'x-amz-request-id' not in notification['responseElements']:
            logger.error('Message does not look like an S3 ObjectCreation notification. Skipping: %s', message)
            return False
    except ValueError as e:
        logger.error("Message missing key (%s). Skipping: %s", e, message_obj.body)
        return False
    return True
