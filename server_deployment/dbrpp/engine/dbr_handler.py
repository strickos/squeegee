from ..config import config
from .utils import make_temp_dir, \
                   delete_temp_directories, \
                   split_file, \
                   download_s3_file_to_local, \
                   process_split_file, \
                   process_splits, \
                   upload_local_file_to_s3, \
                   purger_s3_files_in_dir

from multiprocessing import Pool
from subprocess import call
import csv
import copy
import logging
import os
import re

logger = logging.getLogger(__name__)


def handle_dbr_record(record, business_unit_mappings, default_business_unit_mappings):

    identity = record['responseElements']['x-amz-request-id']
    logging.info("{identity}: Started processing of DBR record.".format(identity=identity))
    try:
        dbr_record_date = dbr_file_date(record['s3']['object']['key'])
        bucket_region = record['awsRegion']

        dbr_path, dbr_filename = download_dbr(record, identity, bucket_region)
        splits_output_path = split_dbr("{0}/{1}".format(dbr_path, dbr_filename), identity)
        header = get_header(splits_output_path, identity)
        processed_splits_output_path, analytics_files_split_paths = process_splits(splits_output_path, header, business_unit_mappings, default_business_unit_mappings, identity, 'dbr')  # noqa
        joined_file_dir = join_dbr_splits(processed_splits_output_path, dbr_record_date, identity)

        upload_processed_dbr(joined_file_dir, dbr_record_date, identity)
        upload_dbr_splits(analytics_files_split_paths, dbr_record_date, identity)
    except Exception as e:
        logging.exception(
            "{identity}: Something went wrong processing record. Will now clean up temporary files & directories: {error}".format(  # noqa
                identity=identity, error=e))
        raise
    finally:
        # Remove all temporary files
        if config.ENV != 'local':
            delete_temp_directories(identity)
    logging.info("{identity}: Finished processing of DBR record.".format(identity=identity))


def dbr_file_date(key):
    """Returns the date from a DBR filename.

    :param key: S3 Object key name.
    :type key: str
    :returns: Date in YYYY-MM.
    :rtype: str
    """
    pattern = r'{}-aws-billing-detailed-line-items-with-resources-and-tags-(\d{{4}}-\d{{2}}).csv.zip'.format(config.master_payer_account_id)  # noqa
    compiled_pattern = re.compile(pattern)
    tmp_date = compiled_pattern.match(key).group(1)
    return tmp_date


def download_dbr(record, identity, bucket_region):
    """Download the DBR file specified in the record to /home/ubuntu/dbr_process/tmp/

    :param record: SQS message record.
    :param identity: x-amz-request-id used to identify the record being processed.
    :param bucket_region: AWS region the bucket resides in.
    :returns: Temporary directory where DBR was downloaded and the DBR filename.
    :rtype: (str, str)
    """

    bucket = record['s3']['bucket']['name']
    key = record['s3']['object']['key']
    output_path = make_temp_dir(identity)
    local_filename = "{0}/{1}".format(output_path, key)
    download_s3_file_to_local(bucket, key, local_filename, identity, bucket_region)

    return output_path, key


def split_dbr(filename, identity):
    """Unzip and split DBR file.

    :param filename: Filename of DBR zip file.
    :param identity: x-amz-request-id used to identify the record being processed.
    :type filename: str
    :type identity: str
    :returns: The temporary directory where the DBR file has been unzipped.
    :rtype: str
    """
    output_path = make_temp_dir(identity)
    split_file(filename, config.split_line_numbers, output_path, identity, 'zip')

    return output_path


def get_header(input_path, identity):
    """Get header from first DBR split.

    :param input_path: Path where the DBR splits reside.
    :param identity: x-amz-request-id used to identify the record being processed.
    :type input_path: str
    :type identity: str
    :returns: List of header entries.
    :rtype: list
    """

    # Get the header line from the original DBR monolith.
    logging.info("{identity}: Getting header from first split: {input_path}/{header_split}".format(input_path=input_path, identity=identity, header_split='xaa'))  # noqa
    with open("{0}/{1}".format(input_path, 'xaa'), 'r') as fh:
        reader = csv.reader(fh)
        header = next(reader)
    logging.info("{identity}: Finished getting header from first split.".format(identity=identity))

    return header


def join_dbr_splits(input_path, dbr_date, identity):
    """Join the DBR splits into a single file and zip it.

    :param input_path: Path where DBR splits live.
    :param dbr_date: Record of DBR in YYYY-MM.
    :param identity: x-amz-request-id used to identify the record being processed.
    :type input_path: str
    :type dbr_date: str
    :type identity: str
    :returns: Temporary directory where joined DBR file is located.
    :rtype: str
    """
    work_dir = make_temp_dir(identity)
    dbr_filename = "{0}-aws-billing-detailed-line-items-with-resources-and-tags-{1}.csv".format(config.master_payer_account_id,
                                                                                                dbr_date)
    os.chdir(input_path)

    logging.info("{identity}: Started concatenation of {input_path}/* to {work_dir}/{dbr_filename}".format(identity=identity,  # noqa
                                                                                                           input_path=input_path,  # noqa
                                                                                                           work_dir=work_dir,  # noqa
                                                                                                           dbr_filename=dbr_filename))  # noqa
    return_code = call("/bin/cat {input_path}/* >> {work_dir}/{dbr_filename}".format(input_path=input_path,
                                                                                     work_dir=work_dir,
                                                                                     dbr_filename=dbr_filename),
                       shell=True)
    if return_code != 0:
        logging.error("{identity}: Failed to concatenate splits into DBR file. Working directory was {work_dir}. Output filename was {filename}. Return code was {return_code}. Exiting.".format(identity=identity,  # noqa
                                                                                                                                                                                                 return_code=return_code,  # noqa
                                                                                                                                                                                                 work_dir=work_dir,  # noqa
                                                                                                                                                                                                 filename=dbr_filename))  # noqa
        raise Exception('ConcatFileError')
    logging.info("{identity}: Finished concatenation of {input_path}/* to {work_dir}/{dbr_filename}".format(identity=identity,  # noqa
                                                                                                            input_path=input_path,  # noqa
                                                                                                            work_dir=work_dir,  # noqa
                                                                                                            dbr_filename=dbr_filename))  # noqa

    os.chdir(work_dir)
    logging.info("{identity}: Started zipping of {work_dir}/{dbr_filename} to {work_dir}/{dbr_filename}.zip".format(identity=identity,  # noqa
                                                                                                                    work_dir=work_dir,  # noqa
                                                                                                                    dbr_filename=dbr_filename))  # noqa
    return_code = call("/usr/bin/zip {work_dir}/{dbr_filename}.zip {dbr_filename}".format(work_dir=work_dir,
                                                                                          dbr_filename=dbr_filename),
                       shell=True)
    if return_code != 0:
        logging.error("{identity}: Failed to zip DBR file. Working directory was {work_dir}. Output filename was {filename}.zip. Return code was {return_code}. Exiting.".format(identity=identity,  # noqa
                                                                                                                                                                                 return_code=return_code,  # noqa
                                                                                                                                                                                 work_dir=work_dir,  # noqa
                                                                                                                                                                                 filename=dbr_filename))  # noqa
        raise Exception('ZipFileError')
    logging.info("{identity}: Finishing zipping of {work_dir}/{dbr_filename} to {work_dir}/{dbr_filename}.zip".format(identity=identity,  # noqa
                                                                                                                      work_dir=work_dir,  # noqa
                                                                                                                      dbr_filename=dbr_filename))  # noqa

    return work_dir


def upload_processed_dbr(input_path, dbr_date, identity):
    """Upload processed DBR file to S3.

    :param input_path: Path where processed DBR lives.
    :param dbr_date: DBR Record date in YYYY-MM format.
    :param identity: x-amz-request-id used to identify the record being processed.
    :type input_path: str
    :type dbr_date: str
    :type identity: str
    :returns: None
    :rtype: None
    """

    filename = "{0}-aws-billing-detailed-line-items-with-resources-and-tags-{1}.csv.zip".format(config.master_payer_account_id, dbr_date)  # noqa
    upload_local_file_to_s3(config.billing_files_output_bucket,
                            filename,
                            "{0}/{1}".format(input_path, filename),
                            identity,
                            config.billing_files_role_arn,
                            config.s3_bucket_region)


def upload_dbr_splits(splits_path, dbr_record_date, identity):
    """Upload DBR splits to analytics bucket.

    :param splits_path: Path where the DBR splits are located.
    :param dbr_record_date: DBR Record date in YYYY-MM format.
    :param identity: x-amz-request-id used to identify the record being processed.
    :type splits_path: str
    :type dbr_record_date: str
    :type identity: str
    :returns: None
    :rtype: None
    """
    logging.info("{identity}: Started purger of analytics DBR files.".format(identity=identity))
    prefix = 'dbr/date={}'.format(dbr_record_date)
    if purger_s3_files_in_dir(config.analytics_files_output_bucket,
                              prefix,
                              identity,
                              config.analytics_files_role_arn,
                              config.s3_bucket_region):
        logging.info("{identity}: S3:{bucket}/{prefix} has been cleaned up".format(
            identity=identity,
            bucket=config.analytics_files_output_bucket,
            prefix=prefix))
    else:
        logging.warning("{identity}: failed to clear up S3:{bucket}/{prefix}".format(
            identity=identity,
            bucket=config.analytics_files_output_bucket,
            prefix=prefix))

    logging.info("{identity}: Uploading DBR splits to analytics bucket s3://{output_bucket}/dbr/date={date}".format(identity=identity,  # noqa
                                                                                                                   output_bucket=config.analytics_files_output_bucket,  # noqa
                                                                                                                   date=dbr_record_date))  # noqa
    for filename in [file for file in os.listdir(splits_path) if file.startswith('x')]:
        key = "dbr/date={0}/{1}".format(dbr_record_date, filename)
        tmp_filename = "{0}/{1}".format(splits_path, filename)
        upload_local_file_to_s3(config.analytics_files_output_bucket,
                                key,
                                tmp_filename,
                                identity,
                                config.analytics_files_role_arn,
                                config.s3_bucket_region)
    logging.info("{identity}: Finished uploading DBR splits to analytics bucket s3://{output_bucket}/dbr/date={date}".format(identity=identity,  # noqa
                                                                                                                            output_bucket=config.analytics_files_output_bucket,  # noqa
                                                                                                                            date=dbr_record_date))  # noqa


def is_dbr_put_notification(record):
    """Check S3 Put SQS message for DBR file drop.

    :param record: S3 Put message to process.
    :type record: dict
    :returns: True if DBR file Drop Put notification
    :rtype: bool
    """
    try:
        if not record['s3']['object']['key'].startswith('{}-aws-billing-detailed-line-items-with-resources-and-tags-'.format(config.master_payer_account_id)):  # noqa
            return False
    except ValueError:
        logger.error("Message is not valid. Skipping.")
        return False
    return True
