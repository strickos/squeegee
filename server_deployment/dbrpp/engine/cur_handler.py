from ..config import config
from .utils import compress_file,\
                   decode_json,\
                   delete_temp_directories,\
                   download_s3_file_to_local,\
                   download_s3_files_to_local,\
                   make_temp_dir,\
                   split_file,\
                   process_splits,\
                   uncompress_file,\
                   upload_local_file_to_s3, \
                   purger_s3_files_in_dir
import json
import logging
import os
import re


logger = logging.getLogger(__name__)


def compress_cur_files(cur_files_path, identity):
    """Compress CUR files in directory

    :param cur_files_path: Path where the CUR uncompressed files are located.
    :param identity: x-amz-request-id used to identify the record being processed.
    :type cur_files_path: str
    :type identity: str
    :returns: None
    :rtype: None
    """
    logging.info("{identity}: Started compress of cur files".format(identity=identity))
    for filename in [file for file in os.listdir(cur_files_path) if file.endswith('csv')]:
        compress_file('{}/{}'.format(cur_files_path, filename), identity)
    logging.info("{identity}: Finished compress of cur files".format(identity=identity))


def create_new_manifest(manifest, manifest_key, identity):
    """Create new manifest file

    :param manifest: Path where processed CUR files are.
    :param manifest_key: Name of new manifest file
    :param identity: x-amz-request-id used to identify the record being processed.
    :type manifest: dict
    :type manifest_key: str
    :type identity: str
    :returns: Path to new file, name of new file
    :rtype: tuple
    """
    logging.info("{identity}: Started create new manifest".format(identity=identity))
    output_path = make_temp_dir(identity)
    manifest_full_path = '{}/{}'.format(output_path, manifest_key)
    manifest['columns'] = config.cur_selected_columns_manifest
    with open(manifest_full_path, 'w') as out_file:
        out_file.write(json.dumps(manifest, indent=4))
    logging.info("{identity}: Finished create new manifest".format(identity=identity))
    return output_path


def cur_file_date(key):
    """Get date of the CUR file in format YYYY-DD.

    :param key: CUR object key name
    :type key: str
    :returns: date string in format YYYY-DD
    :rtype: str
    """
    g = re.match(r'.*/([0-9]{4})([0-9]{2})[0-9]{2}-[0-9]{8}/.*\.json', key)
    if not g:
        return '1970-01'
    return '{}-{}'.format(g.group(1), g.group(2))


def download_manifest_file(bucket, manifest_key, bucket_region, identity):
    """Download CUR files from S3

    :param bucket: Name of the S3 bucket containing the CUR files
    :param manifest_key: Object key name of manifest file
    :param bucket_region: Region of the bucket containing manifest
    :param identity: x-amz-request-id used to identify the record being processed.
    :type bucket: str
    :type manifest_key: str
    :type bucket_region: str
    :type identity: str
    :returns: path to manifest file
    :rtype: str
    """
    logging.info("{identity}: Started download manifest".format(identity=identity))
    temp_dir = make_temp_dir(identity)
    local_filename = '{}/{}'.format(temp_dir, manifest_key.split('/')[-1])
    download_s3_file_to_local(bucket,
                              manifest_key,
                              local_filename,
                              identity,
                              bucket_region)
    logging.info("{identity}: Finished download manifest".format(identity=identity))
    return local_filename


def download_cur_files(bucket_name, keys, bucket_region, identity):
    """Download CUR files from S3

    :param bucket_name: Name of the S3 bucket containing the CUR files
    :param keys: List of object keys to download
    :param bucket_region: Region of the bucket containing manifest
    :param identity: x-amz-request-id used to identify the record being processed.
    :type bucket_name: str
    :type keys: list
    :type bucket_region: str
    :type identity: str
    :returns: path to directory containing the CUR files
    :rtype: str
    """
    logging.info("{identity}: Started download of cur files.".format(identity=identity))
    local_files_dir = make_temp_dir(identity)
    download_s3_files_to_local(bucket_name, keys, local_files_dir, identity, bucket_region)
    logging.info("{identity}: Finished download of cur files.".format(identity=identity))
    return local_files_dir


def handle_cur_message(record, business_unit_mappings, default_business_unit_mappings):
    """Handle the CUR File S3 Put SQS message.

    :param record: S3 Put notification dict
    :param business_unit_mappings: Bad BU mapping objects
    :param default_business_unit_mappings: Default BU mapping objects
    :type record: dict
    :type business_unit_mappings: dict
    :type default_business_unit_mappings: dict
    :returns: None
    :rtype: None
    """
    # Get manifest details from S3 PUT Notification
    bucket_name = record['s3']['bucket']['name']
    manifest_key = record['s3']['object']['key']
    bucket_region = record['awsRegion']

    if not record['s3']['object']['key'].endswith('hourly_with_resources-Manifest.json'):
        logger.error('Found PUT notification for file {} - not a CUR manifest skipping'.format(manifest_key))
        return

    identity = record['responseElements']['x-amz-request-id']
    if re.match(r'.*/[0-9]{8}-[0-9]{8}/[^/]*-Manifest\.json', record['s3']['object']['key']):
        logger.info('{identity}: Top level Manifest file. Skipping as we will receive a folder level manifest file also'.format(identity=identity))  # noqa
        return

    logging.info("{identity}: Started processing of CUR record.".format(identity=identity))

    # Download manifest from S3 Bucket and decode JSON
    manifest_file = download_manifest_file(bucket_name, manifest_key, bucket_region, identity)
    manifest = read_manifest(manifest_file, identity)
    header = ['{}/{}'.format(col['category'], col['name']) for col in manifest['columns']]

    # Locate the CUR files from manifest data and download objects from S3
    # UnGzip and Split CUR files (NOTE: We assume files are gzipped here)
    cur_files_path = download_cur_files(bucket_name, manifest['reportKeys'], bucket_region, identity)
    uncompress_cur_files(cur_files_path, identity)

    processed_cur_file_splits_path, analytics_files_splits_path = process_splits(cur_files_path,
                                                                                 header,
                                                                                 business_unit_mappings,
                                                                                 default_business_unit_mappings,
                                                                                 identity,
                                                                                 'cur')

    upload_cur_splits(analytics_files_splits_path, cur_file_date(manifest_key), identity)
    compress_cur_files(processed_cur_file_splits_path, identity)
    upload_processed_cur_files(processed_cur_file_splits_path, manifest['reportKeys'], identity)

    new_manifest_file_path = create_new_manifest(manifest, manifest_key.split('/')[-1], identity)
    upload_local_file_to_s3(config.billing_files_output_bucket,
                            manifest_key,
                            "{0}/{1}".format(new_manifest_file_path, manifest_key.split('/')[-1]),
                            identity,
                            config.billing_files_role_arn,
                            config.s3_bucket_region)
    key_parts = manifest_key.split('/')
    key_parts.pop(-2)

    top_manifest_key = '/'.join(key_parts)
    upload_local_file_to_s3(config.billing_files_output_bucket,
                            top_manifest_key,
                            "{0}/{1}".format(new_manifest_file_path, manifest_key.split('/')[-1]),
                            identity,
                            config.billing_files_role_arn,
                            config.s3_bucket_region)

    # Remove all temporary files
    if config.ENV != 'local':
        delete_temp_directories(identity)
    logging.info("{identity}: Finished processing of CUR record.".format(identity=identity))


def is_cur_put_notification(record):
    """Check S3 Put SQS message for CUR file drop.

    :param record: S3 PUT message
    :type record: dict
    :returns: True if CUR file Drop Put notification
    :rtype: bool
    """
    try:
        if not record['s3']['object']['key'].endswith('hourly_with_resources-Manifest.json'):
            return False
    except ValueError:
        logger.error("Message is not valid. Skipping.")
        return False
    return True


def read_manifest(manifest_file, identity):
    """Open manifest file and decode json

    :param manifest_file: path to manifest file to open
    :param identity: x-amz-request-id used to identify the record being processed.
    :type manifest_file: str
    :type identity: str
    :returns: Decoded manifest
    :rtype: dict
    """
    logging.info("{identity}: Started read manifest.".format(identity=identity))
    with open(manifest_file, 'r') as manifest_fd:
        manifest_raw_data = manifest_fd.read()
    manifest = (decode_json(manifest_raw_data))
    logging.info("{identity}: Finished read manifest.".format(identity=identity))
    return manifest


def split_cur_files(cur_files_path, identity):
    """Split CUR files into multiple smaller files

    :param cur_files_path: path to the CUR files to split
    :param identity: x-amz-request-id used to identify the record being processed.
    :type cur_files_path: str
    :type identity: str
    :returns: path to directory containing the new split files
    :rtype: str
    """
    logging.info("{identity}: Started splitting of cur files.".format(identity=identity))
    output_path = make_temp_dir(identity)
    files = []
    for filename in os.listdir(cur_files_path):
        files.append('{}/{}'.format(cur_files_path, filename))
    split_file(files, config.split_line_numbers, output_path, identity, compression='gzip')
    logging.info("{identity}: Finished splitting of cur files.".format(identity=identity))
    return output_path


def uncompress_cur_files(cur_files_path, identity):
    """Uncompress CUR files in directory

    :param cur_files_path: Path where the CUR compressed files are located.
    :param identity: x-amz-request-id used to identify the record being processed.
    :type cur_files_path: str
    :type identity: str
    :returns: None
    :rtype: None
    """
    logging.info("{identity}: Started uncompress of cur files".format(identity=identity))
    for filename in [file for file in os.listdir(cur_files_path) if file.endswith('gz')]:
        uncompress_file('{}/{}'.format(cur_files_path, filename), identity)
    logging.info("{identity}: Finished uncompress of cur files".format(identity=identity))


def upload_cur_splits(splits_path, record_date, identity):
    """Upload CUR splits to Analytics bucket.

    :param splits_path: Path where the CUR splits are located.
    :param record_date: CUR Record date in YYYY-MM format.
    :param identity: x-amz-request-id used to identify the record being processed.
    :type splits_path: str
    :type record_date: str
    :type identity: str
    :returns: None
    :rtype: None
    """
    logging.info("{identity}: Started purger of analytics cur files.".format(identity=identity))
    prefix = 'cost_usage_reports/billingperiodstartdate={}'.format(record_date)
    if purger_s3_files_in_dir(config.analytics_files_output_bucket,
                           prefix,
                           identity,
                           config.analytics_files_role_arn,
                           config.s3_bucket_region):
        logging.info("{identity}: S3:{bucket}/{prefix} has been cleaned up".format(
                     identity=identity,
                     bucket=config.analytics_files_output_bucket,
                     prefix=prefix))
    else:
        logging.warning("{identity}: failed to clear up S3:{bucket}/{prefix}".format(
                     identity=identity,
                     bucket=config.analytics_files_output_bucket,
                     prefix=prefix))

    logging.info("{identity}: Started upload of analytics cur files.".format(identity=identity))
    for filename in [file for file in os.listdir(splits_path) if file.endswith('csv')]:
        key = 'cost_usage_reports/billingperiodstartdate={}/{}'.format(record_date, filename)
        local_filename = '{}/{}'.format(splits_path, filename)
        upload_local_file_to_s3(config.analytics_files_output_bucket,
                                key,
                                local_filename,
                                identity,
                                config.analytics_files_role_arn,
                                config.s3_bucket_region)
    logging.info("{identity}: Finished upload of analytics cur files.".format(identity=identity))


def upload_processed_cur_files(input_path, cur_key_names, identity):
    """Upload processed CUR files to S3.

    :param input_path: Path where processed CUR files are.
    :param cur_key_names: List of CUR file keynames from manifest.
    :param identity: x-amz-request-id used to identify the record being processed.
    :type input_path: str
    :type cur_key_names: list
    :type identity: str
    :returns: None
    :rtype: None
    """
    logging.info("{identity}: Started upload of processed cur files.".format(identity=identity))
    for key_name in cur_key_names:
        filename = key_name.split('/')[-1]
        upload_local_file_to_s3(config.billing_files_output_bucket,
                                key_name,
                                "{0}/{1}".format(input_path, filename),
                                identity,
                                config.billing_files_role_arn,
                                config.s3_bucket_region)
    logging.info("{identity}: Finished upload of processed cur files.".format(identity=identity))
