from ..config import config
from .cur_handler import handle_cur_message, is_cur_put_notification, cur_file_date
from .dbr_handler import handle_dbr_record, is_dbr_put_notification, dbr_file_date
from .utils import copy_cost_allocation_file,\
                   current_year_month,\
                   decode_json,\
                   delete_message,\
                   get_resource,\
                   load_business_unit_mappings,\
                   load_default_business_unit_mappings,\
                   validate_message,\
                   delete_temp_directories,\
                   copy_file_to_monthly_directory
from json import JSONDecodeError
import logging


logger = logging.getLogger(__name__)


def handle_message(message):
    """Handle the SQS message.

    :param message: SQS message to process.
    :type message: message object
    :returns: None
    :rtype: None
    """
    delete_temp_directories(identity='', temp_dir=config.temp_dir, full_cleanup=True)
    try:
        logger.info('Handle Message')
        if not validate_message(message):
            # TODO: Consider if we delete this message
            logger.warning('Message not a valid S3 PUT notification for a Billing Report File.')
            return
        message_dict = decode_json(message.body)
        for record in message_dict['Records']:
            try:
                identity = record['responseElements']['x-amz-request-id']
                if 'userIdentity' in record:
                    if 'principalId' in record['userIdentity']:
                        logger.info('{identity}: Message received from {principal}'.format(identity=identity,
                                                                                           principal=record['userIdentity']['principalId']))  # noqa
                handle_function = None
                message_date = None

                if is_cur_put_notification(record):
                    message_date = cur_file_date
                    handle_function = handle_cur_message
                elif is_dbr_put_notification(record):
                    message_date = dbr_file_date
                    handle_function = handle_dbr_record
                else:
                    logger.warning('Unknown message type: %s', message_dict)

                if handle_function and message_date:
                    current_date = current_year_month()
                    record_date = message_date(record['s3']['object']['key'])

                    if record_date == current_date:
                        copy_file_to_monthly_directory(current_date, identity, config.accounts_file)
                        copy_file_to_monthly_directory(current_date, identity, config.business_unit_mapping_file)
                        copy_cost_allocation_file(current_date, record['s3']['bucket']['name'], identity)

                    business_unit_mappings = load_business_unit_mappings(record_date, identity)
                    default_business_unit_mappings = load_default_business_unit_mappings(record_date, identity)

                    handle_function(record, business_unit_mappings, default_business_unit_mappings)
            except Exception as e:
                logger.exception("Failed to handle record! %s", e)
        delete_message(message)
    except JSONDecodeError:
        logger.warning('Message cannot be coded as JSON: %s', message.body)
    except Exception as e:
        logger.exception("Failed to handle message! %s", e)


def loop():
    """Loop over messages in the queue.

    :returns: None
    :rtype: None
    """
    sqs = get_resource(service='sqs', region_name=config.sqs_queue_region)
    queue = sqs.Queue(config.sqs_queue_url)
    while True:
        messages = queue.receive_messages(MaxNumberOfMessages=config.sqs_max_number_of_messages,
                                          VisibilityTimeout=config.sqs_visibility_timeout,
                                          WaitTimeSeconds=config.sqs_wait_timeout)
        for message in messages:
            handle_message(message)


def worker():
    """Worker loop.

    :returns: None
    :rtype: None
    """
    try:
        loop()
    except Exception as e:
        logger.exception("Main loop failed! %s", e)
