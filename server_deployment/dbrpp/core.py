"""
Core object initialisation
"""


import logging.config

from .api.app import create_app
from .engine.worker import worker
from .config import config
from .lib.process import worker_processes
from .server import run_app

# Initialize logger
logging.config.dictConfig(config.logging_config)

# Global objects
app = create_app()

# Project entry-point, is lifted in __init__.py
def start_server():
    with worker_processes(target=worker, proc_count=config.proc_count) as worker_pids:
        config.WORKER_PIDS = worker_pids
        run_app(app)