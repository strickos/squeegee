"""
REST frontend to CASE Database
"""
import logging
import psutil
from flask import Blueprint, Flask, jsonify, current_app, render_template
from werkzeug.contrib.fixers import ProxyFix
from ..config import config

logger = logging.getLogger(__name__)


def create_app(cfg=None):
    app = Flask(__name__)
    app.config.update(cfg or {})

    app.wsgi_app = ProxyFix(app.wsgi_app)

    app.debug = config.DEBUG

    app.register_blueprint(root)

    return app


# -------------- Root views -------------- #

root = Blueprint('root', __name__)


@root.route('/healthcheck')
def healthcheck():
    """Micros Healthcheck"""
    try:
        procs = [psutil.Process(pid).status() for pid in config.WORKER_PIDS]
    except:
        logger.critical('Could not find worker process')
        return 'Unhealthy: could not find worker process', 500
    return 'Healthy: {}'.format(procs), 200


@root.route('/version')
def version():
    """Return some version info about the app"""
    version_info = config.version_info()
    return jsonify(**version_info), 200


@root.route('/')
def index():
    """Redirect to api root"""
    # result = {
    #     'urls': sorted([
    #         i.rule for i in current_app.url_map.iter_rules()
    #         if i.rule.startswith('/api')
    #     ])
    # }

    # return jsonify(result), 200
    return render_template('index.html')
