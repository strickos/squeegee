"""
CASE Gunicorn Server
"""

import logging
import gunicorn.app.base
import gunicorn.glogging
from .config import config

logger = logging.getLogger(__name__)


class StandaloneApplication(gunicorn.app.base.Application):
    def __init__(self, app, options=None):
        self.options = options or {}
        self.application = app
        super().__init__()

    def init(self, parser, opts, args):
        pass

    def load_config(self):
        for key, value in self.options.items():
            self.cfg.set(key.lower(), value)

    def load(self):
        return self.application


class Logger(gunicorn.glogging.Logger):
    """
    We have to override the setup method of the gunicorn logger
    because you cannot configure it without a config file
    initialize and then load the config from the global
    logging.yaml config file
    """

    def setup(self, cfg):
        super().setup(cfg)
        logging.config.dictConfig(config.logging_config)


def run_app(app):
    StandaloneApplication(app, config.gunicorn_options).run()
