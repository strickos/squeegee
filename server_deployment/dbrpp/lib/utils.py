import logging
from ..config import config

logger = logging.getLogger(__name__)


def setup_logging():
    logging.config.dictConfig(config.logging_config)

