"""
Handles logging formats.
"""
import json
import logging
import os
import threading
import pendulum

from colorlog import ColoredFormatter

# local thread data for thread safe logging
extra_info = threading.local()


class UTCFormatter(logging.Formatter):
    """
    Formatter that always logs time in UTC.
    """

    def formatTime(self, record, datefmt=None):
        """
        Override default to use strftime, e.g. to get microseconds.
        """
        created = pendulum.from_timestamp(record.created, 'UTC')
        if datefmt:
            return created.strftime(datefmt)
        else:
            return str(created)


class JSONFormatter(UTCFormatter):
    """
    A formatter that renders log records as JSON objects.
    You can add extra fields to the record like so::

        logger.info('message', extra={'extra': {'custom_field': 'value'}})

    which will result in::

        {
            "custom_field": "value",
            "level": "INFO",
            "message": "message",
            "name": "some.logger",
            "timestamp": "2015-09-28T05:04:16.268.394947+0000"
        }
    """

    def format(self, record):
        """
        Encode log record as JSON. Add any additional fields in the ``extra``
        attribute of ``record`` as objects in the returned JSON object. Also
        adds any additional fields in the :data:`extra_info` thread local data
        in the same way.

        :param dict record: standard Python log record.

        :returns: JSON representation of a log line.
        :rtype: string
        """

        log = {
            "timestamp": self.formatTime(record, self.datefmt),
            "level": record.levelname,
            "name": record.name,
            "message": record.getMessage(),
            "pid": os.getpid()
        }

        if 'extra' in record.__dict__:
            for key, value in record.__dict__['extra'].items():
                # log[key] = str(record.__dict__['extra'][key])
                log[key] = str(value)

        for key, value in extra_info.__dict__.items():
            log[key] = str(value)

        if record.exc_info:
            log["traceback"] = self.formatException(record.exc_info)
        return json.dumps(log, sort_keys=True)


def get_formatter(json_logging=None):
    """
    Return the type of logging formatter that should be used for the app.
    """
    if json_logging is None:
        json_logging = 'NO_JSON_LOGGING' not in os.environ
    if not json_logging:
        return ColoredFormatter
    return JSONFormatter


Formatter = get_formatter()
