"""
Context managers for easily starting and cleaning up multiple processes
"""

from contextlib import contextmanager, ExitStack
from multiprocessing import Process


@contextmanager
def worker_process(*args, **kwargs):
    workerd = Process(*args, **kwargs)
    workerd.start()
    yield workerd.pid
    workerd.join()


@contextmanager
def worker_processes(*args, proc_count=1, **kwargs):
    with ExitStack() as stack:
        pids = []
        for proc in range(proc_count):
            workerd = worker_process(*args, **kwargs)
            pid = stack.enter_context(workerd)
            pids.append(pid)
        yield pids