from dbrpp.lib.utils import setup_logging
from dbrpp import start_server


if __name__ == '__main__':
    setup_logging()
    start_server()
