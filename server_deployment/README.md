![DBR-PP](docs/images/readme/dbr-pp2_thumb.png)

# Detailed Billing Reports - Preprocessing Pipeline (DBR-PP)

The DBR-PP is a service that will download AWS Detailed Billing Reports ([DBR-RT][DBR-RT-DOCS] and [CUR][CUR-DOCS] files), process them using *customisable* advanced cost allocation rules, then upload them to a new output S3 bucket. In the default form the DBR-PP will create a new cost allocation tag called *cost_allocation* and append it to each billing line of your detailed billing report. The value for the *cost_allocation* tag will be either the value of another cost allocation tag *business_unit* or a default business unit which is mapped against the account id.

![Design-Overview](docs/images/readme/DBR-PP-HLD.png)

## Installation
A quickstart cloudformation has been provided to assist you in getting the DBR-PP up and running. This quickstart is not intended as the only way to configure the DBR-PP but as a method of demoing the solution and providing a sample configuration to base your own deployment from. It is recommended to you set the DBRPP up in a development account initially to test the solution out before granting it access to your live billing data.

### Pre-requisites
* S3 Bucket (InputBillingFilesBucket) that will receive the AWS Billing files from AWS (or one you can upload them into manually)
* S3 Bucket (OutputBillingFilesBucket) that the DBR-PP can upload the processed AWS Billing Files
* S3 Bucket (AnalyticsBillingFilesBucket) that the DBR-PP can upload the analytics optimised AWS Billing Files
* S3 Bucket (MappingFilesBucket) that contains the mapping files (This can be shared with the analytics bucket)
* VPC with at least 1 subnet for the DBR-PP instance to run inside
* EC2 Access Key to apply to the DBR-PP instance

### Quickstart Deployment
1. Deploy the [cloudformation stack][CLOUDFORMATION-FILE]
    * Open the [CloudFormation Console][CLOUDFORMATION-CONSOLE]
    * Select "Create Stack"
    * Upload a template to Amazon S3 (Choose File from your local machine)
    * Select "Next"
    * Set Stack Name to something like "dbr-pp" or a name that matches your corporate policy
    * Parameters
      * S3 Configuration
        * InputBillingFilesBucket - Bucket name that receives the original AWS billing files
        * OutputBillingFilesBucket - Bucket name that will receive the processed AWS billing files
        * AnalyticsBillingFilesBucket - Bucket name that will receive the analytics optimised billing files
        * MappingFilesBucket - Bucket name that contains the mapping files for processing
      * Role Configuration
        * InstanceRoleName - Name of the IAM Role to create for the EC2 instance, with access to SQS and STS
        * BillingFilesRoleName - Name of the IAM Role that will have access to the InputBillingFilesBucket
        * AnalyticsFilesRoleName - Name of the IAM Role that will have access to the AnalyticsBillingFilesBucket and MappingFilesBucket
      * Instance Configuration
        * InstanceVPC - VPCId that the DBR-PP EC2 instance will be deployed
        * InstanceSubnets - SubnetId(s) of the subnets the EC2 instance will be deployed
        * InstanceKeyPair - The EC2 KeyPair name to be deployed onto the EC2 instance
        * InstanceSize - The size of the EC2 instance (eg. t2.xlarge, c4.large)
        * InstanceStorageType -  Type of EBS storage (eg. gp2, io1)
        * InstanceStorageSize - The size of the EBS volume given to the EC2 instance
        * InstanceStorageIops - For io1 volume type what PIOPs to provision
        * InstanceMonitoring - Enable EC2 detailed monitoring
        * ImageId - The AMI to use for the EC2 image, if set to 'default' it will use a default Amazon Linux AMI.
      * Tagging Configuration
        * TagBusinessUnit - Value to apply to the 'business_unit' tag on resources
        * TagResourceOwner - Value to apply to the 'resource_owner' tag on resources
        * TagServiceName - Value to apply to the 'service_name' tag on resources
      * Application Configuration
        * DbrppCodeUrl - URL to the DBR-PP zip file
        * DbrppAppPort - Port to run DBR-PP on
        * DbrppSplitLineCount - Number of lines to split the DBR-RT file on
2. Update the SNS Topic SQS Subscription to RAW ([needed until this issue is resolved.](https://bitbucket.org/atlassian/dbr-pp/issues/3/add-support-for-non-raw-subscriptions-on))
    * Open [SNS Console](https://console.aws.amazon.com/sns/v2/home)
    * Select "Topics"
    * Click the ARN for the "DBRPP-Notification" Topic
    * Tick the SQS Subscription
    * Select dropdown "Other subscription actions"
    * Choose "Edit subscription attributes"
    * Set "Raw message delivery" on
3. Setup S3 Event Notifications for InputBillingFilesBucket
    * Open [S3 Console](https://console.aws.amazon.com/s3/home)
    * Select the InputBullingFilesBucket
    * Select "Properties"
    * Under "Advanced Settings" section select "Events"
    * Click "Add notification" (For Detailed Billing Report with Resources and Tags)
      * Name: DBR-RT-put
      * Events: ObjectCreate (All)
      * Prefix: 1234567890-aws-billing-detailed-line-items-with-resources-and-tags-" (NOTE: Update the leading number with your Master Payer Account Id)
      * Suffix: .csv.zip
      * Send To: SNS Topic
      * SNS: DBRPP-Notification
      * ![Event-Setup-DBR](docs/images/readme/S3_Event_DBR.png)
    * Click "Save"
    * Open "Events" dialog again
    * Click "Add notification" (For Cost and Usage Reports)
      * Name: CostReport-put
      * Events: ObjectCreate (All)
      * Prefix: (This will depend on how you setup the CUR, but you should put the prefix (Base Folder) value for your CUR)
      * Suffix: .json
      * Send To: SNS Topic
      * SNS: DBRPP-Notification
      * ![Event-Setup-CUR](docs/images/readme/S3_Event_CUR.png)
4. Upload mapping files to your MappingFilesBucket
    * Open [S3 Console](https://console.aws.amazon.com/s3/home)
    * Select the MappingFilesBucket
    * Select "Create folder"
    * Create a new folder called "mapping"
    * Select the new folder
    * Upload both mapping.csv and accounts.json (Samples to come)

## Tests
TBC

## Contributors
Pull requests, issues and comments welcome. For pull requests:

* Add tests for new features and bug fixes
* Follow the existing style
* Separate unrelated changes into multiple pull requests

See the existing issues for things to start contributing.

For bigger changes, make sure you start a discussion first by creating
an issue and explaining the intended change.

Atlassian requires contributors to sign a Contributor License Agreement,
known as a CLA. This serves as a record stating that the contributor is
entitled to contribute the code/documentation/translation to the project
and is willing to have it used in distributions and derivative works
(or is willing to transfer ownership).

Prior to accepting your contributions we ask that you please follow the appropriate
link below to digitally sign the CLA. The Corporate CLA is for those who are
contributing as a member of an organization and the individual CLA is for
those contributing as an individual.

* [CLA for corporate contributors](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=e1c17c66-ca4d-4aab-a953-2c231af4a20b)
* [CLA for individuals](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=3f94fbdc-2fbe-46ac-b14c-5d152700ae5d)

## License
Copyright (c) 2016 Atlassian and others. Apache 2.0 licensed, see [LICENSE][LICENSE-FILE] file. Open Source Approval: OSR-286

   [DBR-RT-DOCS]: <http://docs.aws.amazon.com/awsaccountbilling/latest/aboutv2/billing-reports.html#reportstagsresources>
   [CUR-DOCS]:  <http://docs.aws.amazon.com/awsaccountbilling/latest/aboutv2/billing-reports.html#enhanced-reports>
   [CONSOLIDATED-BILLING-DOCS]: <http://docs.aws.amazon.com/organizations/latest/userguide/orgs_getting-started_from-consolidatedbilling.html>
   [LICENSE-FILE]: <https://bitbucket.org/atlassian/dbr-pp/src/master/LICENSE>
   [CLOUDFORMATION-FILE]: <https://bitbucket.org/atlassian/dbr-pp/src/master/quickstart/dbr-pp-cloudformation.yaml>
   [CLOUDFORMATION-CONSOLE]: <https://console.aws.amazon.com/cloudformation/home>
  
   
   
   