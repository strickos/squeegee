from pyathena import connect
from pytest import approx
from decimal import Decimal


def run_athena_query(query):
    cursor = connect(s3_staging_dir='s3://squeegee-ci-output/athena/').cursor()
    cursor.execute(query)
    return cursor.fetchall()


def test_all_line_count():
    result = run_athena_query("SELECT count(*) FROM squeegeeci.cost_usage_report")
    assert(result == [(13732456,)])


def test_usage_line_count():
    result = run_athena_query("SELECT count(*) FROM squeegeeci.cost_usage_report WHERE lineitem_lineitemtype = 'Usage'")
    assert(result == [(13518986,)])


def test_discounted_usage_line_count():
    result = run_athena_query("SELECT count(*) FROM squeegeeci.cost_usage_report WHERE lineitem_lineitemtype = 'DiscountedUsage'")
    assert(result == [(213364,)])


def test_rifee_line_count():
    result = run_athena_query("SELECT count(*) FROM squeegeeci.cost_usage_report WHERE lineitem_lineitemtype = 'RIFee'")
    assert(result == [(104,)])


def test_credit_line_count():
    result = run_athena_query("SELECT count(*) FROM squeegeeci.cost_usage_report WHERE lineitem_lineitemtype = 'Credit'")
    assert(result == [(0,)])


def test_usage_line_sum():
    result = run_athena_query("SELECT SUM(CAST(lineitem_unblendedcost AS DECIMAL(30,15))) FROM squeegeeci.cost_usage_report WHERE lineitem_lineitemtype = 'Usage'")
    value = result[0][0]
    assert(value == approx(Decimal('209187.1271410863')))

